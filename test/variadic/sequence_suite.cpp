///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/sequence.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

template <typename T, size_t N>
struct test_array;

template <typename... T>
struct test_tuple;

//-----------------------------------------------------------------------------

namespace suite_sequence
{

static_assert(variadic::sequence<0>::at(0) == 0, "");
static_assert(variadic::sequence<1>::at(0) == 1, "");

static_assert(variadic::sequence<0, 1>::at(0) == 0, "");
static_assert(variadic::sequence<0, 1>::at(1) == 1, "");

static_assert(variadic::sequence<1, 0>::at(0) == 1, "");
static_assert(variadic::sequence<1, 0>::at(1) == 0, "");

} // namespace suite_sequence

//-----------------------------------------------------------------------------

namespace suite_sequence_size
{

static_assert(variadic::sequence_size<int>::value == 0, "");

// Tuple

static_assert(variadic::sequence_size<test_tuple<>>::value == 0, "");
static_assert(variadic::sequence_size<test_tuple<int>>::value == 1, "");
static_assert(variadic::sequence_size<test_tuple<int, int>>::value == 2, "");
static_assert(variadic::sequence_size<test_tuple<int, int, int>>::value == 3, "");

// Array

static_assert(variadic::sequence_size<test_array<int, 0>>::value == 0, "");
static_assert(variadic::sequence_size<test_array<int, 1>>::value == 1, "");
static_assert(variadic::sequence_size<test_array<int, 2>>::value == 2, "");
static_assert(variadic::sequence_size<test_array<int, 3>>::value == 3, "");

static_assert(variadic::sequence_size<int[1]>::value == 1, "");
static_assert(variadic::sequence_size<int[2]>::value == 2, "");
static_assert(variadic::sequence_size<int[3]>::value == 3, "");

} // namespace suite_sequence_size

//-----------------------------------------------------------------------------

namespace suite_enumerate_sequence
{

static_assert(is_same<variadic::enumerate_sequence<0>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<1>, variadic::sequence<0>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<2>, variadic::sequence<0, 1>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<3>, variadic::sequence<0, 1, 2>>::value, "");

static_assert(is_same<variadic::enumerate_sequence<0, 10>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<1, 10>, variadic::sequence<10>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<2, 10>, variadic::sequence<10, 11>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<3, 10>, variadic::sequence<10, 11, 12>>::value, "");

static_assert(is_same<variadic::enumerate_sequence<0, 0, 2>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<1, 0, 2>, variadic::sequence<0>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<2, 0, 2>, variadic::sequence<0, 2>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<3, 0, 2>, variadic::sequence<0, 2, 4>>::value, "");

static_assert(is_same<variadic::enumerate_sequence<0, 10, 2>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<1, 10, 2>, variadic::sequence<10>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<2, 10, 2>, variadic::sequence<10, 12>>::value, "");
static_assert(is_same<variadic::enumerate_sequence<3, 10, 2>, variadic::sequence<10, 12, 14>>::value, "");

} // namespace suite_enumerate_sequence

//-----------------------------------------------------------------------------

namespace suite_adjoin_sequence_each
{

// One sequence

static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<0>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<0>, variadic::sequence<1>>,
                      variadic::sequence<1>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10>, variadic::sequence<0>>,
                      variadic::sequence<10>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10>, variadic::sequence<1>>,
                      variadic::sequence<11>>::value,
              "");

// Two sequences

static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<0, 0>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<0, 0>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<0, 0>, variadic::sequence<0>, variadic::sequence<1>>,
                      variadic::sequence<0, 1>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10, 0>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<10, 0>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10, 0>, variadic::sequence<0>, variadic::sequence<1>>,
                      variadic::sequence<10, 1>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10, 20>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<10, 20>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<10, 20>, variadic::sequence<0>, variadic::sequence<1>>,
                      variadic::sequence<10, 21>>::value,
              "");

// Three sequences

static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<0, 0, 0>, variadic::sequence<0>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<0, 0, 0>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence_each<variadic::sequence<1, 10, 100>, variadic::sequence<0>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<1, 10, 100>>::value,
              "");

} // namespace suite_adjoin_sequence_each

//-----------------------------------------------------------------------------

namespace suite_adjoin_sequence
{

// One sequence

static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>>,
                      variadic::sequence<0>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0, 1>>,
                      variadic::sequence<0, 1>>::value,
              "");

// Two sequences

static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<0, 1>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<1>>,
                      variadic::sequence<0, 2>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<0, 0>>,
                      variadic::sequence<0, 1, 1>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<1, 2>>,
                      variadic::sequence<0, 2, 3>>::value,
              "");

// Three sequences

static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<0>, variadic::sequence<0>>,
                      variadic::sequence<0, 1, 2>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<10>, variadic::sequence<100>>,
                      variadic::sequence<0, 11, 102>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<0, 0>, variadic::sequence<0, 0, 0>>,
                      variadic::sequence<0, 1, 1, 2, 2, 2>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<10, 20>, variadic::sequence<100>>,
                      variadic::sequence<0, 11, 21, 102>>::value,
              "");
static_assert(is_same<variadic::adjoin_sequence<variadic::sequence<0>, variadic::sequence<10, 20>, variadic::sequence<100, 200>>,
                      variadic::sequence<0, 11, 21, 102, 202>>::value,
              "");

} // namespace suite_adjoin_sequence

//-----------------------------------------------------------------------------

namespace suite_sequence_row
{

static_assert(is_same<variadic::sequence_row<1, 1>, variadic::sequence<0>>(), "");
static_assert(is_same<variadic::sequence_row<1, 2>, variadic::sequence<0, 0>>(), "");
static_assert(is_same<variadic::sequence_row<1, 3>, variadic::sequence<0, 0, 0>>(), "");

static_assert(is_same<variadic::sequence_row<2, 1>, variadic::sequence<0, 1>>(), "");
static_assert(is_same<variadic::sequence_row<2, 2>, variadic::sequence<0, 1, 0, 1>>(), "");
static_assert(is_same<variadic::sequence_row<2, 3>, variadic::sequence<0, 1, 0, 1, 0, 1>>(), "");

static_assert(is_same<variadic::sequence_row<3, 1>, variadic::sequence<0, 1, 2>>(), "");
static_assert(is_same<variadic::sequence_row<3, 2>, variadic::sequence<0, 1, 2, 0, 1, 2>>(), "");
static_assert(is_same<variadic::sequence_row<3, 3>, variadic::sequence<0, 1, 2, 0, 1, 2, 0, 1, 2>>(), "");

} // namespace suite_sequence_row

//-----------------------------------------------------------------------------

namespace suite_sequence_column
{

static_assert(is_same<variadic::sequence_column<1, 1>, variadic::sequence<0>>(), "");
static_assert(is_same<variadic::sequence_column<1, 2>, variadic::sequence<0, 1>>(), "");
static_assert(is_same<variadic::sequence_column<1, 3>, variadic::sequence<0, 1, 2>>(), "");

static_assert(is_same<variadic::sequence_column<2, 1>, variadic::sequence<0, 0>>(), "");
static_assert(is_same<variadic::sequence_column<2, 2>, variadic::sequence<0, 0, 1, 1>>(), "");
static_assert(is_same<variadic::sequence_column<2, 3>, variadic::sequence<0, 0, 1, 1, 2, 2>>(), "");

static_assert(is_same<variadic::sequence_column<3, 1>, variadic::sequence<0, 0, 0>>(), "");
static_assert(is_same<variadic::sequence_column<3, 2>, variadic::sequence<0, 0, 0, 1, 1, 1>>(), "");
static_assert(is_same<variadic::sequence_column<3, 3>, variadic::sequence<0, 0, 0, 1, 1, 1, 2, 2, 2>>(), "");

} // namespace suite_sequence_column

//-----------------------------------------------------------------------------

namespace suite_sequence_column_select
{

static_assert(is_same<variadic::sequence_column_select<1, variadic::enumerate_sequence<1>>,
                      variadic::sequence<0>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<1, variadic::enumerate_sequence<2>>,
                      variadic::sequence<0, 1>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<1, variadic::enumerate_sequence<3>>,
                      variadic::sequence<0, 1, 2>>(),
              "");

static_assert(is_same<variadic::sequence_column_select<2, variadic::enumerate_sequence<1>>,
                      variadic::sequence<0, 0>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<2, variadic::enumerate_sequence<2>>,
                      variadic::sequence<0, 0, 1, 1>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<2, variadic::enumerate_sequence<3>>,
                      variadic::sequence<0, 0, 1, 1, 2, 2>>(),
              "");

static_assert(is_same<variadic::sequence_column_select<3, variadic::enumerate_sequence<1>>,
                      variadic::sequence<0, 0, 0>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<3, variadic::enumerate_sequence<2>>,
                      variadic::sequence<0, 0, 0, 1, 1, 1>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<3, variadic::enumerate_sequence<3>>,
                      variadic::sequence<0, 0, 0, 1, 1, 1, 2, 2, 2>>(),
              "");

static_assert(is_same<variadic::sequence_column_select<1, variadic::sequence<11, 22, 33>>,
                      variadic::sequence<11, 22, 33>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<2, variadic::sequence<11, 22, 33>>,
                      variadic::sequence<11, 11, 22, 22, 33, 33>>(),
              "");
static_assert(is_same<variadic::sequence_column_select<3, variadic::sequence<11, 22, 33>>,
                      variadic::sequence<11, 11, 11, 22, 22, 22, 33, 33, 33>>(),
              "");

} // namespace suite_sequence_column_select

//-----------------------------------------------------------------------------

namespace suite_sequence_of
{

static_assert(is_same<variadic::sequence_of<int[1]>, variadic::sequence<0>>::value, "");
static_assert(is_same<variadic::sequence_of<int[2]>, variadic::sequence<0, 1>>::value, "");
static_assert(is_same<variadic::sequence_of<int[3]>, variadic::sequence<0, 1, 2>>::value, "");

static_assert(is_same<variadic::sequence_of<test_array<int, 0>>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::sequence_of<test_array<int, 1>>, variadic::sequence<0>>::value, "");
static_assert(is_same<variadic::sequence_of<test_array<int, 2>>, variadic::sequence<0, 1>>::value, "");
static_assert(is_same<variadic::sequence_of<test_array<int, 3>>, variadic::sequence<0, 1, 2>>::value, "");

static_assert(is_same<variadic::sequence_of<test_tuple<>>, variadic::sequence<>>::value, "");
static_assert(is_same<variadic::sequence_of<test_tuple<int>>, variadic::sequence<0>>::value, "");
static_assert(is_same<variadic::sequence_of<test_tuple<int, int>>, variadic::sequence<0, 1>>::value, "");
static_assert(is_same<variadic::sequence_of<test_tuple<int, int, int>>, variadic::sequence<0, 1, 2>>::value, "");

} // namespace suite_sequence_of

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
