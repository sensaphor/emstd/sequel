///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/logical.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_logical_and
{

static_assert(variadic::logical_and(false) == false, "");
static_assert(variadic::logical_and(true) == true, "");

static_assert(variadic::logical_and(false, false) == false, "");
static_assert(variadic::logical_and(false, true) == false, "");
static_assert(variadic::logical_and(true, false) == false, "");
static_assert(variadic::logical_and(true, true) == true, "");

static_assert(variadic::logical_and(false, false, false) == false, "");
static_assert(variadic::logical_and(true, false, false) == false, "");
static_assert(variadic::logical_and(true, true, false) == false, "");
static_assert(variadic::logical_and(true, true, true) == true, "");

} // namespace suite_logical_and

//-----------------------------------------------------------------------------

namespace suite_logical_or
{

static_assert(variadic::logical_or(false) == false, "");
static_assert(variadic::logical_or(true) == true, "");

static_assert(variadic::logical_or(false, false) == false, "");
static_assert(variadic::logical_or(false, true) == true, "");
static_assert(variadic::logical_or(true, false) == true, "");
static_assert(variadic::logical_or(true, true) == true, "");

static_assert(variadic::logical_or(false, false, false) == false, "");
static_assert(variadic::logical_or(true, false, false) == true, "");
static_assert(variadic::logical_or(true, true, false) == true, "");
static_assert(variadic::logical_or(true, true, true) == true, "");

} // namespace suite_logical_or

//-----------------------------------------------------------------------------

namespace suite_logical_none
{

static_assert(variadic::logical_none(false) == true, "");
static_assert(variadic::logical_none(true) == false, "");

static_assert(variadic::logical_none(false, false) == true, "");
static_assert(variadic::logical_none(false, true) == false, "");
static_assert(variadic::logical_none(true, false) == false, "");
static_assert(variadic::logical_none(true, true) == false, "");

static_assert(variadic::logical_none(false, false, false) == true, "");
static_assert(variadic::logical_none(true, false, false) == false, "");
static_assert(variadic::logical_none(true, true, false) == false, "");
static_assert(variadic::logical_none(true, true, true) == false, "");

} // namespace suite_logical_none

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
