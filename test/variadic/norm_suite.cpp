///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/norm.hpp>
#include <emstd/functional.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_max
{

static_assert(variadic::max(11) == 11, "");
static_assert(variadic::max(22, 11) == 22, "");
static_assert(variadic::max(33, 22, 11) == 33, "");
static_assert(variadic::max(44, 33, 22, 11) == 44, "");

static_assert(variadic::max(11, 22, 33) == 33, "");
static_assert(variadic::max(11, 33, 22) == 33, "");
static_assert(variadic::max(22, 11, 33) == 33, "");
static_assert(variadic::max(22, 33, 11) == 33, "");
static_assert(variadic::max(33, 11, 22) == 33, "");
static_assert(variadic::max(33, 22, 11) == 33, "");

// Mixed

static_assert(variadic::max(33.f, 22, 11) == 33.f, "");
static_assert(variadic::max(33, 22.f, 11) == 33.f, "");
static_assert(variadic::max(33, 22, 11.f) == 33.f, "");

} // namespace suite_max

//-----------------------------------------------------------------------------

namespace suite_partial_max
{

void partial_max_variables()
{
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_max(ra, rb, rc, 11, 22, 33);
        assert(ra == 11);
        assert(rb == 22);
        assert(rc == 33);
    }
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_max(ra, rb, rc, 33, 22, 11);
        assert(ra == 33);
        assert(rb == 33);
        assert(rc == 33);
    }
}

void run()
{
    partial_max_variables();
}

} // namespace suite_partial_max

//-----------------------------------------------------------------------------

namespace suite_min
{

static_assert(variadic::min(44) == 44, "");
static_assert(variadic::min(44, 33) == 33, "");
static_assert(variadic::min(44, 33, 22) == 22, "");
static_assert(variadic::min(44, 33, 22, 11) == 11, "");

static_assert(variadic::min(11, 22, 33) == 11, "");
static_assert(variadic::min(11, 33, 22) == 11, "");
static_assert(variadic::min(22, 11, 33) == 11, "");
static_assert(variadic::min(22, 33, 11) == 11, "");
static_assert(variadic::min(33, 11, 22) == 11, "");
static_assert(variadic::min(33, 22, 11) == 11, "");

// Mixed

static_assert(variadic::min(33.f, 22, 11) == 11.f, "");
static_assert(variadic::min(33, 22.f, 11) == 11.f, "");
static_assert(variadic::min(33, 22, 11.f) == 11.f, "");

} // namespace suite_min

//-----------------------------------------------------------------------------

namespace suite_partial_min
{

void partial_min_variables()
{
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_min(ra, rb, rc, 11, 22, 33);
        assert(ra == 11);
        assert(rb == 11);
        assert(rc == 11);
    }
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_min(ra, rb, rc, 33, 22, 11);
        assert(ra == 33);
        assert(rb == 22);
        assert(rc == 11);
    }
}

void run()
{
    partial_min_variables();
}

} // namespace suite_partial_min

//-----------------------------------------------------------------------------

namespace suite_minmax
{

static_assert(variadic::minmax(11, 22, 33).min() == 11, "");
static_assert(variadic::minmax(11, 22, 33).max() == 33, "");
static_assert(variadic::minmax(33, 22, 11).min() == 11, "");
static_assert(variadic::minmax(33, 22, 11).max() == 33, "");

// Mixed

static_assert(variadic::minmax(11.f, 22, 33).min() == 11.f, "");
static_assert(variadic::minmax(11.f, 22, 33).max() == 33.f, "");
static_assert(variadic::minmax(11, 22.f, 33).min() == 11.f, "");
static_assert(variadic::minmax(11, 22.f, 33).max() == 33.f, "");
static_assert(variadic::minmax(11, 22, 33.f).min() == 11.f, "");
static_assert(variadic::minmax(11, 22, 33.f).max() == 33.f, "");

} // namespace suite_minmax

//-----------------------------------------------------------------------------

int main()
{
    suite_partial_max::run();
    suite_partial_min::run();
    return 0;
}
