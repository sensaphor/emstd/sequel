///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/get.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_get
{

static_assert(variadic::get(index_constant<0>{}, 11) == 11, "");
static_assert(variadic::get(index_constant<0>{}, 11, 22) == 11, "");
static_assert(variadic::get(index_constant<0>{}, 11, 22, 33) == 11, "");
static_assert(variadic::get(index_constant<1>{}, 11, 22) == 22, "");
static_assert(variadic::get(index_constant<1>{}, 11, 22, 33) == 22, "");
static_assert(variadic::get(index_constant<2>{}, 11, 22, 33) == 33, "");

static_assert(variadic::get(index_constant<0>{}, true, 2, 3.0) == true, "");
static_assert(variadic::get(index_constant<1>{}, true, 2, 3.0) == 2, "");
static_assert(variadic::get(index_constant<2>{}, true, 2, 3.0) == 3.0, "");

static_assert(is_same<decltype(variadic::get(index_constant<0>{}, true, 2, 3.0)), bool>(), "");
static_assert(is_same<decltype(variadic::get(index_constant<1>{}, true, 2, 3.0)), int>(), "");
static_assert(is_same<decltype(variadic::get(index_constant<2>{}, true, 2, 3.0)), double>(), "");

void element_reference()
{
    int array[]{ 11, 22, 33, 44 };
    assert(variadic::get(index_constant<0>{}, array[0], array[1], array[2], array[3]) == 11);
    assert((is_same<decltype(variadic::get(index_constant<0>{}, array[0], array[1], array[2], array[3])), int&>()));
    assert((is_same<decltype(variadic::get(index_constant<1>{}, array[0], array[1], array[2], array[3])), int&>()));
    assert((is_same<decltype(variadic::get(index_constant<2>{}, array[0], array[1], array[2], array[3])), int&>()));
    assert((is_same<decltype(variadic::get(index_constant<3>{}, array[0], array[1], array[2], array[3])), int&>()));
}

void run()
{
    element_reference();
}

} // namespace suite_get

//-----------------------------------------------------------------------------

int main()
{
    suite_get::run();
    return 0;
}
