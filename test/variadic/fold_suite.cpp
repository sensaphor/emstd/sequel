///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/fold.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_fold
{

// Add

static_assert(variadic::fold(fox::add, 11) == 11, "");
static_assert(variadic::fold(fox::add, 11, 22) == 33, "");
static_assert(variadic::fold(fox::add, 11, 22, 33) == 66, "");

static_assert(variadic::fold(fox::add, 11.f) == 11.f, "");
static_assert(variadic::fold(fox::add, 11.f, 22.f) == 33.f, "");
static_assert(variadic::fold(fox::add, 11.f, 22.f, 33.f) == 66.f, "");

// Add mixed
static_assert(variadic::fold(fox::add, 11.f, 22) == 33.f, "");
static_assert(variadic::fold(fox::add, 11, 22.f) == 33.f, "");

// Max

static_assert(variadic::fold(fox::max, 11) == 11, "");
static_assert(variadic::fold(fox::max, 11, 22) == 22, "");
static_assert(variadic::fold(fox::max, 11, 22, 33) == 33, "");

// Min

static_assert(variadic::fold(fox::min, 11) == 11, "");
static_assert(variadic::fold(fox::min, 11, 22) == 11, "");
static_assert(variadic::fold(fox::min, 11, 22, 33) == 11, "");

// Accumulate

constexpr auto test_accumulate = core::bind_front(variadic::fold, fox::add);

static_assert(test_accumulate(11) == 11, "");
static_assert(test_accumulate(11, 22) == 33, "");
static_assert(test_accumulate(11, 22, 33) == 66, "");

static_assert(test_accumulate(11.f) == 11.f, "");
static_assert(test_accumulate(11.f, 22.f) == 33.f, "");
static_assert(test_accumulate(11.f, 22.f, 33.f) == 66.f, "");

// Accumulate mixed
static_assert(test_accumulate(11, 22.f) == 33.f, "");
static_assert(test_accumulate(11.f, 22) == 33.f, "");
static_assert(test_accumulate(11.f, 22, 33) == 66.f, "");

} // namespace suite_fold

//-----------------------------------------------------------------------------

namespace suite_fold_over
{

static_assert(variadic::fold_over(fox::add, fox::abs, -11) == 11, "");
static_assert(variadic::fold_over(fox::add, fox::abs, -11, 22) == 33, "");
static_assert(variadic::fold_over(fox::add, fox::abs, -11, 22, -33) == 66, "");
static_assert(variadic::fold_over(fox::add, fox::abs, -11, 22, -33, 44) == 110, "");

static_assert(variadic::fold_over(fox::add, fox::subtract, 11, 22) == -11, "");
static_assert(variadic::fold_over(fox::add, fox::subtract, 11, 22, -33, -44) == 0, "");

static_assert(variadic::fold_over(fox::add, fox::subtract, variadic::arity_2, 11, 22) == -11, "");
static_assert(variadic::fold_over(fox::add, fox::subtract, variadic::arity_2, 11, 22, -33, -44) == 0, "");

static_assert(variadic::fold_over(fox::add, fox::multiply, 11, 22) == 242, "");
static_assert(variadic::fold_over(fox::add, fox::multiply, 11, 22, 33, 44) == 242 + 1452, "");

static_assert(variadic::fold_over(fox::add, fox::multiply, variadic::arity_2, 11, 22) == 242, "");
static_assert(variadic::fold_over(fox::add, fox::multiply, variadic::arity_2, 11, 22, 33, 44) == 242 + 1452, "");

void absolute_sum_int()
{
    const int array[]{ -11, 22, -33, 44 };
    assert(variadic::fold_over(fox::add, fox::abs, array[0]) == 11);
    assert(variadic::fold_over(fox::add, fox::abs, array[0], array[1]) == 33);
    assert(variadic::fold_over(fox::add, fox::abs, array[0], array[1], array[2]) == 66);
    assert(variadic::fold_over(fox::add, fox::abs, array[0], array[1], array[2], array[3]) == 110);
}

void diff_sum_int()
{
    const int array[]{ 11, 22, -33, -44 };
    assert(variadic::fold_over(fox::add, fox::subtract, array[0], array[1]) == -11);
    assert(variadic::fold_over(fox::add, fox::subtract, array[0], array[1], array[2], array[3]) == 0);
}

void inner_product_int()
{
    {
        constexpr auto test_inner_product = core::bind_front(variadic::fold_over, fox::add, fox::multiply);
        constexpr int array[]{ 11, 22, 33, 44 };
        static_assert(test_inner_product(array[0], array[1]) == 11 * 22, "");
        static_assert(test_inner_product(array[0], array[1], array[2], array[3]) == 11 * 22 + 33 * 44, "");
        static_assert(test_inner_product(variadic::arity_2, array[0], array[1], array[2], array[3]) == 11 * 22 + 33 * 44, "");
    }
    {
        constexpr auto test_product = core::bind_front(variadic::fold, fox::multiply);
        constexpr auto test_inner_product = core::bind_front(variadic::fold_over, fox::add, test_product, variadic::arity_2);
        constexpr int array[]{ 11, 22, 33, 44 };
        static_assert(test_inner_product(array[0], array[1]) == 11 * 22, "");
        static_assert(test_inner_product(array[0], array[1], array[2], array[3]) == 11 * 22 + 33 * 44, "");
    }
}

void run()
{
    absolute_sum_int();
    diff_sum_int();
    inner_product_int();
}

} // namespace suite_fold_over

//-----------------------------------------------------------------------------

int main()
{
    suite_fold_over::run();
    return 0;
}
