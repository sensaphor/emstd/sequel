///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/tuple_transform.hpp>
#include <tuple>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/fox/norm.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_tuple_transform_index
{

void unary_tuple()
{
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::arity_1, core::index_sequence<0>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 33);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::arity_1, core::index_sequence<0, 2>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == -33);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::arity_1, core::index_sequence<0, 1, 2>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == -22);
        assert(ext::get<2>(tuple) == -33);
    }
    // Implicit arity
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::index_sequence<0>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 33);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::index_sequence<0, 2>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == -33);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform_index(fox::negate, core::index_sequence<0, 1, 2>{}, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == -22);
        assert(ext::get<2>(tuple) == -33);
    }
}

void unary_array()
{
    {
        int array[]{ 11, 22, 33 };
        variadic::tuple_transform_index(fox::negate, core::arity_1, core::index_sequence<0, 2>{}, array);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == -33);
    }
    // Implicit arity
    {
        int array[]{ 11, 22, 33 };
        variadic::tuple_transform_index(fox::negate, core::index_sequence<0>{}, array);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == 33);
    }
    {
        int array[]{ 11, 22, 33 };
        variadic::tuple_transform_index(fox::negate, core::index_sequence<0, 2>{}, array);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == -33);
    }
}

void binary_tuple()
{
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33, 44);
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0>{}, tuple);
        assert(ext::get<0>(tuple) == 33);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 33);
        assert(ext::get<3>(tuple) == 44);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33, 44);
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<1>{}, tuple);
        assert(ext::get<0>(tuple) == 11);
        assert(ext::get<1>(tuple) == 55);
        assert(ext::get<2>(tuple) == 33);
        assert(ext::get<3>(tuple) == 44);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33, 44);
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<2>{}, tuple);
        assert(ext::get<0>(tuple) == 11);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 77);
        assert(ext::get<3>(tuple) == 44);
    }
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33, 44);
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 2>{}, tuple);
        assert(ext::get<0>(tuple) == 33);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 77);
        assert(ext::get<3>(tuple) == 44);
    }
}

void binary_array()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0>{}, array);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<2>{}, array);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 2>{}, array);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
}

void binary_array_output()
{
    // Front results
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 2>{}, core::index_sequence<0, 2>{}, array);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    // Back results
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 2>{}, core::index_sequence<1, 3>{}, array);
        assert(array[0] == 11);
        assert(array[1] == 33);
        assert(array[2] == 33);
        assert(array[3] == 77);
    }
}

void adjacent_fold()
{
    // Adjacent sum
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 1, 2>{}, array);
        assert(array[0] == 33);
        assert(array[1] == 55);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    // Adjacent max
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<0, 1, 2>{}, array);
        assert(array[0] == 22);
        assert(array[1] == 33);
        assert(array[2] == 44);
        assert(array[3] == 44);
    }
}

void partial_sum()
{
    // Partial right fold sum
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<2, 1, 0>{}, array);
        assert(array[0] == 110);
        assert(array[1] == 99);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44, 55, 66 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<4, 3, 2, 1, 0>{}, array);
        assert(array[0] == 231);
        assert(array[1] == 220);
        assert(array[2] == 198);
        assert(array[3] == 165);
        assert(array[4] == 121);
        assert(array[5] == 66);
    }
    // Partial left fold sum with overlapping slices
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 1, 2>{}, core::index_sequence<1, 2, 3>{}, array);
        assert(array[0] == 11);
        assert(array[1] == 33);
        assert(array[2] == 66);
        assert(array[3] == 110);
    }
    {
        int array[]{ 11, 22, 33, 44, 55 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<0, 1, 2, 3>{}, core::index_sequence<1, 2, 3, 4>{}, array);
        assert(array[0] == 11);
        assert(array[1] == 33);
        assert(array[2] == 66);
        assert(array[3] == 110);
        assert(array[4] == 165);
    }
}

void partial_max()
{
    // Partial right fold
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<2, 1, 0>{}, array);
        assert(array[0] == 44);
        assert(array[1] == 44);
        assert(array[2] == 44);
        assert(array[3] == 44);
    }
    {
        int array[]{ 44, 33, 22, 11 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<2, 1, 0>{}, array);
        assert(array[0] == 44);
        assert(array[1] == 33);
        assert(array[2] == 22);
        assert(array[3] == 11);
    }
    {
        int array[]{ 11, 99, 33, 44 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<2, 1, 0>{}, array);
        assert(array[0] == 99);
        assert(array[1] == 99);
        assert(array[2] == 44);
        assert(array[3] == 44);
    }
    // Partial left fold
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<0, 1, 2>{}, core::index_sequence<1, 2, 3>{}, array);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 44, 33, 22, 11 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<0, 1, 2>{}, core::index_sequence<1, 2, 3>{}, array);
        assert(array[0] == 44);
        assert(array[1] == 44);
        assert(array[2] == 44);
        assert(array[3] == 44);
    }
    {
        int array[]{ 44, 33, 99, 11 };
        variadic::tuple_transform_index(fox::max, core::arity_2, core::index_sequence<0, 1, 2>{}, core::index_sequence<1, 2, 3>{}, array);
        assert(array[0] == 44);
        assert(array[1] == 44);
        assert(array[2] == 99);
        assert(array[3] == 99);
    }
}

void sketchpad_fold()
{
    // Fold using front slice as sketchpad
    {
        int array[]{ 0, 0, 11, 22, 33, 44 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<2, 4, 0>{}, core::index_sequence<0, 1, 0>{}, array);
        assert(array[0] == 110); // Last partial sum
        assert(array[1] == 77); // Second-last partial sum
        assert(array[2] == 11);
        assert(array[3] == 22);
        assert(array[4] == 33);
        assert(array[5] == 44);
    }
    {
        int array[]{ 0, 0, 11, 22, 33, 44, 55, 66 };
        variadic::tuple_transform_index(fox::add, core::arity_2, core::index_sequence<2, 4, 0, 6, 0>{}, core::index_sequence<0, 1, 0, 1, 0>{}, array);
        assert(array[0] == 231);
        assert(array[1] == 121);
        assert(array[2] == 11);
        assert(array[3] == 22);
        assert(array[4] == 33);
        assert(array[5] == 44);
        assert(array[6] == 55);
        assert(array[7] == 66);
    }
}

void run()
{
    unary_tuple();
    unary_array();
    binary_array();
    binary_array_output();
    adjacent_fold();
    partial_sum();
    partial_max();
    sketchpad_fold();
}

} // namespace suite_tuple_transform_index

//-----------------------------------------------------------------------------

namespace suite_tuple_transform
{

void unary_tuple()
{
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform(fox::negate, core::arity_1, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == -22);
        assert(ext::get<2>(tuple) == -33);
    }
    // Implicit arity
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33);
        variadic::tuple_transform(fox::negate, tuple);
        assert(ext::get<0>(tuple) == -11);
        assert(ext::get<1>(tuple) == -22);
        assert(ext::get<2>(tuple) == -33);
    }
}

void unary_array()
{
    {
        int array[]{ 11, 22, 33 };
        variadic::tuple_transform(fox::negate, core::arity_1, array);
        assert(array[0] == -11);
        assert(array[1] == -22);
        assert(array[2] == -33);
    }
    // Implicit arity
    {
        int array[]{ 11, 22, 33 };
        variadic::tuple_transform(fox::negate, array);
        assert(array[0] == -11);
        assert(array[1] == -22);
        assert(array[2] == -33);
    }
}

void binary_tuple()
{
    {
        auto tuple = core::make_tuple_rebind<std::tuple>(11, 22, 33, 44);
        variadic::tuple_transform(fox::add, core::arity_2, tuple);
        assert(ext::get<0>(tuple) == 33);
        assert(ext::get<1>(tuple) == 22);
        assert(ext::get<2>(tuple) == 77);
        assert(ext::get<3>(tuple) == 44);
    }
}

void binary_array()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::tuple_transform(fox::add, core::arity_2, array);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44, 55, 66 };
        variadic::tuple_transform(fox::add, core::arity_2, array);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
        assert(array[4] == 121);
        assert(array[5] == 66);
    }
}

void run()
{
    unary_tuple();
    unary_array();
    binary_tuple();
    binary_array();
}

} // namespace suite_tuple_transform

//-----------------------------------------------------------------------------

int main()
{
    suite_tuple_transform_index::run();
    suite_tuple_transform::run();
    return 0;
}
