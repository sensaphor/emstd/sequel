///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/arithmetic.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_add
{

static_assert(variadic::add(11) == 11, "");
static_assert(variadic::add(22, 11) == 33, "");
static_assert(variadic::add(33, 22, 11) == 66, "");
static_assert(variadic::add(44, 33, 22, 11) == 110, "");

// Mixed
static_assert(variadic::add(33.0, 22, 11) == 66.0, "");
static_assert(variadic::add(33, 22.0, 11) == 66.0, "");
static_assert(variadic::add(33, 22, 11.0) == 66.0, "");

} // namespace suite_add

//-----------------------------------------------------------------------------

namespace suite_subtract
{

static_assert(variadic::subtract(11) == 11, "");
static_assert(variadic::subtract(33, 22) == 11, "");
static_assert(variadic::subtract(66, 33, 22) == 11, "");
static_assert(variadic::subtract(110, 44, 33, 22) == 11, "");

// Mixed
static_assert(variadic::subtract(66.0, 33, 22) == 11.0, "");
static_assert(variadic::subtract(66, 33.0, 22) == 11.0, "");
static_assert(variadic::subtract(66, 33, 22.0) == 11.0, "");

} // namespace suite_subtract

//-----------------------------------------------------------------------------

namespace suite_multiply
{

static_assert(variadic::multiply(11) == 11, "");
static_assert(variadic::multiply(22, 11) == 242, "");
static_assert(variadic::multiply(33, 22, 11) == 7986, "");
static_assert(variadic::multiply(44, 33, 22, 11) == 351384, "");

// Mixed
static_assert(variadic::multiply(33.0, 22, 11) == 7986.0, "");
static_assert(variadic::multiply(33, 22.0, 11) == 7986.0, "");
static_assert(variadic::multiply(33, 22, 11.0) == 7986.0, "");

} // namespace suite_multiply

//-----------------------------------------------------------------------------

namespace suite_add_over
{

static_assert(variadic::add_over(fox::negate, 11) == -11, "");
static_assert(variadic::add_over(fox::negate, 22, 11) == -22 + -11, "");
static_assert(variadic::add_over(fox::negate, 33, 22, 11) == -33 + -22 + -11, "");

static_assert(variadic::add_over(fox::multiply, 22, 11) == 22 * 11, "");
static_assert(variadic::add_over(fox::multiply, 44, 33, 22, 11) == 44 * 33 + 22 * 11, "");

} // namespace suite_add_over

//-----------------------------------------------------------------------------

namespace suite_subtract_over
{

static_assert(variadic::subtract_over(fox::negate, 11) == -11, "");
static_assert(variadic::subtract_over(fox::negate, 22, 11) == -22 - -11, "");
static_assert(variadic::subtract_over(fox::negate, 33, 22, 11) == -33 - -22 - -11, "");

static_assert(variadic::subtract_over(fox::multiply, 22, 11) == 22 * 11, "");
static_assert(variadic::subtract_over(fox::multiply, 44, 33, 22, 11) == 44 * 33 - 22 * 11, "");

} // namespace suite_subtract_over

//-----------------------------------------------------------------------------

namespace suite_multiply_over
{

static_assert(variadic::multiply_over(fox::negate, 11) == -11, "");
static_assert(variadic::multiply_over(fox::negate, 22, 11) == -22 * -11, "");
static_assert(variadic::multiply_over(fox::negate, 33, 22, 11) == -33 * -22 * -11, "");

static_assert(variadic::multiply_over(fox::subtract, 22, 11) == 22 - 11, "");
static_assert(variadic::multiply_over(fox::subtract, 44, 33, 22, 11) == (44 - 33) * (22 - 11), "");

} // namespace suite_multiply_over

//-----------------------------------------------------------------------------

namespace suite_dot_product
{

static_assert(variadic::dot_product(11, 22) == 11 * 22, "");
static_assert(variadic::dot_product(11, 22, 33, 44) == 11 * 22 + 33 * 44, "");
static_assert(variadic::dot_product(11, 22, 33, 44, 55, 66) == 11 * 22 + 33 * 44 + 55 * 66, "");

static_assert(variadic::dot_product(11, 1.f, 22, 0.5f, 44, 0.25f) == 33.f, "");

} // namespace suite_dot_product

//-----------------------------------------------------------------------------

namespace suite_partial_add
{

void partial_add_variables()
{
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_add(ra, rb, rc, 11, 22, 33);
        assert(ra == 11);
        assert(rb == 11 + 22);
        assert(rc == 11 + 22 + 33);
    }
}

void run()
{
    partial_add_variables();
}

} // namespace suite_partial_add

//-----------------------------------------------------------------------------

namespace suite_partial_multiply
{

void partial_multiply_variables()
{
    {
        int ra;
        int rb;
        int rc;
        variadic::partial_multiply(ra, rb, rc, 33, 22, 11);
        assert(ra == 33);
        assert(rb == 33 * 22);
        assert(rc == 33 * 22 * 11);
    }
}

void run()
{
    partial_multiply_variables();
}

} // namespace suite_partial_multiply

//-----------------------------------------------------------------------------

int main()
{
    suite_partial_add::run();
    suite_partial_multiply::run();
    return 0;
}
