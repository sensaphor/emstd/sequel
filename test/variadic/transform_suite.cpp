///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/transform.hpp>
#include <emstd/variadic/fold.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_transform_index
{

void unary_local()
{
    {
        int result = 11;
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0>{}, result);
        assert(result == -11);
    }
}

void unary_array()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0, 1>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == -11);
        assert(array[1] == -22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0, 2>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == -33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0, 3>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == -11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == -44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0, 1, 2, 3>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == -11);
        assert(array[1] == -22);
        assert(array[2] == -33);
        assert(array[3] == -44);
    }
}

void binary_local()
{
    {
        int result = 11;
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0>{}, result, 22);
        assert(result == 33);
    }
}

void binary_array()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<1>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 55);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<2>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0, 2>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    // Adjacent
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0, 1>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 55);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0, 1, 2>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 55);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    // Reversed (partial right sum)
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<2, 1, 0>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 110);
        assert(array[1] == 99);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
}

void run()
{
    unary_local();
    unary_array();
    binary_local();
    binary_array();
}

} // namespace suite_transform_index

///-----------------------------------------------------------------------------

namespace suite_transform_index_output
{

void unary_local()
{
    {
        int result{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<1>{}, variadic::sequence<0>{}, result, 11);
        assert(result == -11);
    }
    {
        int result{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0>{}, variadic::sequence<1>{}, 11, result);
        assert(result == -11);
    }
    {
        const int input{ 11 };
        int result{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<1>{}, variadic::sequence<0>{}, result, input);
        assert(result == -11);
    }
    {
        const int input{ 11 };
        int result{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<0>{}, variadic::sequence<1>{}, input, result);
        assert(result == -11);
    }
}

void unary_array()
{
    {
        int result[2]{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<2, 3>{}, variadic::sequence<0, 1>{},
                                  result[0], result[1], 11, 22);
        assert(result[0] == -11);
        assert(result[1] == -22);
    }
    {
        int result[2]{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<1, 3>{}, variadic::sequence<0, 2>{},
                                  result[0], 11, result[1], 22);
        assert(result[0] == -11);
        assert(result[1] == -22);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        int result[4]{};
        variadic::transform_index(fox::negate, variadic::arity_1, variadic::sequence<4, 5, 6, 7>{}, variadic::sequence<0, 1, 2, 3>{},
                                  result[0], result[1], result[2], result[3], array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
}

void binary_local()
{
    {
        int result{};
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<1>{}, variadic::sequence<0>{}, result, 11, 22);
    }
    {
        int result{};
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0>{}, variadic::sequence<2>{}, 11, 22, result);
        assert(result == 33);
    }
}

void binary_array()
{
    // Inplace
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0>{}, variadic::sequence<0>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<1>{}, variadic::sequence<1>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 55);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    // Separate output
    {
        int array[]{ 11, 22, 33, 44 };
        int result[3]{};
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<1>{}, variadic::sequence<0>{},
                                  result[0], array[0], array[1], array[2], array[3]);
        assert(result[0] == 33);
        assert(result[1] == 0);
        assert(result[2] == 0);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        int result[3]{};
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<3, 4, 5>{}, variadic::sequence<0, 1, 2>{},
                                  result[0], result[1], result[2], array[0], array[1], array[2], array[3]);
        assert(result[0] == 33);
        assert(result[1] == 55);
        assert(result[2] == 77);
    }
}

template <typename F, typename... Args>
constexpr voider test_partial_fold(F&& fn, Args&&... args)
{
    return variadic::transform_index(forward_cast<F>(fn),
                                     variadic::arity_2,
                                     variadic::enumerate_sequence<sizeof...(Args) - 1, 0, 1>{},
                                     variadic::enumerate_sequence<sizeof...(Args) - 1, 1, 1>{},
                                     forward_cast<Args>(args)...);
}

void partial_sum()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform_index(fox::add, variadic::arity_2, variadic::sequence<0, 1, 2>{}, variadic::sequence<1, 2, 3>{},
                                  array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 33);
        assert(array[2] == 66);
        assert(array[3] == 110);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        test_partial_fold(fox::add, array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 33);
        assert(array[2] == 66);
        assert(array[3] == 110);
    }
 }
 
void run()
{
    unary_local();
    unary_array();
    binary_local();
    binary_array();
    partial_sum();
}

} // namespace suite_transform_index_output

//-----------------------------------------------------------------------------

namespace suite_transform
{

void unary_abs_int()
{
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform(fox::abs, array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ -11, -22, -33, -44 };
        variadic::transform(fox::abs, array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, -22, 33, -44 };
        variadic::transform(fox::abs, array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
    {
        int array[]{ -11, -22, -33, -44 };
        variadic::transform(fox::abs, variadic::arity_1, array[0], array[1], array[2], array[3]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 44);
    }
}

void binary_add_int()
{
    {
        int array[]{ 11, 22 };
        variadic::transform(fox::add, variadic::arity_2, array[0], array[1]);
        assert(array[0] == 33);
        assert(array[1] == 22);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform(fox::add, variadic::arity_2, array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        int array[]{ 11, 22, 33, 44, 55, 66 };
        variadic::transform(fox::add, variadic::arity_2, array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
        assert(array[4] == 121);
        assert(array[5] == 66);
    }
}

void nary_accumulate_int()
{
    constexpr auto test_accumulate = core::bind_front(variadic::fold, fox::add);
    {
        int array[]{ 11, 22, 33, 44 };
        variadic::transform(test_accumulate, variadic::arity_2, array[0], array[1], array[2], array[3]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
    }
    {
        // Two groups with three elements each
        // [0] = [0] + [1]
        // [1] = [2] + [3]
        // [2] = [4] + [5]
        int array[]{ 11, 22, 33, 44, 55, 66 };
        variadic::transform(test_accumulate, variadic::arity_2, array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 77);
        assert(array[3] == 44);
        assert(array[4] == 121);
        assert(array[5] == 66);
    }
    {
        // Three groups with two elements each
        // [0] = [0] + [1] + [2]
        // [1] = [3] + [4] + [5]
        int array[]{ 11, 22, 33, 44, 55, 66 };
        variadic::transform(test_accumulate, variadic::arity_3, array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(array[0] == 66);
        assert(array[1] == 22);
        assert(array[2] == 33);
        assert(array[3] == 165);
        assert(array[4] == 55);
        assert(array[5] == 66);
    }
}

void run()
{
    unary_abs_int();
    binary_add_int();
    nary_accumulate_int();
}

} // namespace suite_transform

//-----------------------------------------------------------------------------

namespace suite_transform_onto
{

void unary_abs_int()
{
    {
        int result{};
        variadic::transform_onto(fox::abs, result, -11);
        assert(result == 11);
    }
    {
        const int input{ -11 };
        int result{};
        variadic::transform_onto(fox::abs, result, input);
        assert(result == 11);
    }
    {
        int result[2]{};
        variadic::transform_onto(fox::abs, result[0], result[1], -11, -22);
        assert(result[0] == 11);
        assert(result[1] == 22);
    }
    {
        int array[]{ -11, -22, -33, -44 };
        int result[4]{};
        variadic::transform_onto(fox::abs, result[0], result[1], result[2], result[3], array[0], array[1], array[2], array[3]);
        assert(result[0] == 11);
        assert(result[1] == 22);
        assert(result[2] == 33);
        assert(result[3] == 44);
    }
}

void binary_add_int()
{
    {
        int result{};
        variadic::transform_onto(fox::add, result, 11, 22);
        assert(result == 33);
    }
    {
        const int array[]{ 11, 22 };
        int result[1]{};
        variadic::transform_onto(fox::add, result[0], array[0], array[1]);
        assert(result[0] == 33);
    }
    {
        const int array[]{ 11, 22, 33, 44 };
        int result[2]{};
        variadic::transform_onto(fox::add, result[0], result[1], array[0], array[1], array[2], array[3]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
    }
    {
        const int array[]{ 11, 22, 33, 44, 55, 66 };
        int result[3]{};
        variadic::transform_onto(fox::add, result[0], result[1], result[2], array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
        assert(result[2] == array[4] + array[5]);
    }
}

void binary_add_arity_int()
{
    {
        int result{};
        variadic::transform_onto(fox::add, variadic::arity_2, result, 11, 22);
        assert(result == 33);
    }
    {
        const int array[]{ 11, 22 };
        int result[1]{};
        variadic::transform_onto(fox::add, variadic::arity_2, result[0], array[0], array[1]);
        assert(result[0] == 33);
    }
    {
        const int array[]{ 11, 22, 33, 44 };
        int result[2]{};
        variadic::transform_onto(fox::add, variadic::arity_2, result[0], result[1], array[0], array[1], array[2], array[3]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
    }
    {
        const int array[]{ 11, 22, 33, 44, 55, 66 };
        int result[3]{};
        variadic::transform_onto(fox::add, variadic::arity_2, result[0], result[1], result[2], array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
        assert(result[2] == array[4] + array[5]);
    }
}

void nary_accumulate_arity_int()
{
    constexpr auto test_accumulate = core::bind_front(variadic::fold, fox::add);
    {
        const int array[]{ 11, 22, 33, 44 };
        int result[2]{};
        variadic::transform_onto(test_accumulate, variadic::arity_2, result[0], result[1], array[0], array[1], array[2], array[3]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
    }
    {
        // Two groups with three elements each
        const int array[]{ 11, 22, 33, 44, 55, 66 };
        int result[3]{};
        variadic::transform_onto(test_accumulate, variadic::arity_2, result[0], result[1], result[2], array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(result[0] == array[0] + array[1]);
        assert(result[1] == array[2] + array[3]);
        assert(result[2] == array[4] + array[5]);
    }
    {
        // Three groups with two elements each
        const int array[]{ 11, 22, 33, 44, 55, 66 };
        int result[2]{};
        variadic::transform_onto(test_accumulate, variadic::arity_3, result[0], result[1], array[0], array[1], array[2], array[3], array[4], array[5]);
        assert(result[0] == array[0] + array[1] + array[2]);
        assert(result[1] == array[3] + array[4] + array[5]);
    }
}

void run()
{
    unary_abs_int();
    binary_add_int();
    binary_add_arity_int();
    nary_accumulate_arity_int();
}

} // namespace suite_transform_onto

//-----------------------------------------------------------------------------

int main()
{
    suite_transform_index::run();
    suite_transform_index_output::run();
    suite_transform::run();
    suite_transform_onto::run();
    return 0;
}
