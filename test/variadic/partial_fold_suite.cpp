///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/partial_fold.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/fox/norm.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_partial_fold
{

void partial_fold_variables()
{
    {
        int a;
        variadic::partial_fold(fox::add, a, 11);
        assert(a == 11);
    }
    {
        int a, b;
        variadic::partial_fold(fox::add, a, b, 11, 22);
        assert(a == 11);
        assert(b == a + 22);
    }
    {
        int a, b, c;
        variadic::partial_fold(fox::add, a, b, c, 11, 22, 33);
        assert(a == 11);
        assert(b == a + 22);
        assert(c == b + 33);
    }
}

void partial_fold_mixed_variables()
{
    {
        double a;
        int b;
        int c;
        variadic::partial_fold(fox::add, a, b, c, 11.5, 22, 33);
        assert(a == 11.5);
        assert(b == 33);
        assert(c == 66);
    }
    {
        int a;
        double b;
        int c;
        variadic::partial_fold(fox::add, a, b, c, 11, 22.5, 33);
        assert(a == 11);
        assert(b == 33.5);
        assert(c == 66);
    }
    {
        int a;
        int b;
        double c;
        variadic::partial_fold(fox::add, a, b, c, 11, 22, 33.5);
        assert(a == 11);
        assert(b == 33);
        assert(c == 66.5);
    }
    {
        double a;
        int b;
        double c;
        variadic::partial_fold(fox::add, a, b, c, 11.5, 22, 33.5);
        assert(a == 11.5);
        assert(b == 33); // Rounded down...
        assert(c == 66.5); // so this is not 77.0
    }
}

void partial_fold_array()
{
    {
        // Partial sum
        int result[3]{};
        variadic::partial_fold(fox::add, result[0], result[1], result[2], 11, 22, 33);
        assert(result[0] == 11);
        assert(result[1] == result[0] + 22);
        assert(result[2] == result[1] + 33);
    }
    {
        // Partial product
        int result[3]{};
        variadic::partial_fold(fox::multiply, result[0], result[1], result[2], 11, 22, 33);
        assert(result[0] == 11);
        assert(result[1] == result[0] * 22);
        assert(result[2] == result[1] * 33);
    }
    {
        // Partial max
        int result[3]{};
        variadic::partial_fold(fox::max, result[0], result[1], result[2], 11, 22, 33);
        assert(result[0] == 11);
        assert(result[1] == 22);
        assert(result[2] == 33);
    }
    {
        // Partial min
        int result[3]{};
        variadic::partial_fold(fox::min, result[0], result[1], result[2], 33, 22, 11);
        assert(result[0] == 33);
        assert(result[1] == 22);
        assert(result[2] == 11);
    }
}

void partial_fold_bind()
{
    {
        constexpr auto partial_sum = core::bind_front(variadic::partial_fold, fox::add);
        int result[3]{};
        partial_sum(result[0], result[1], result[2], 11, 22, 33);
        assert(result[0] == 11);
        assert(result[1] == result[0] + 22);
        assert(result[2] == result[1] + 33);
    }
}

void run()
{
    partial_fold_variables();
    partial_fold_mixed_variables();
    partial_fold_array();
    partial_fold_bind();
}

} // namespace suite_partial_fold

//-----------------------------------------------------------------------------

namespace suite_partial_fold_inplace
{

void partial_fold_variables()
{
    {
        int a = 11;
        int b = 22;
        int c = 33;
        variadic::partial_fold_inplace(fox::add, a, b, c);
        assert(a == 11);
        assert(b == a + 22);
        assert(c == b + 33);
    }
}

void partial_fold_mixed_variables()
{
    {
        double a = 11.5;
        int b = 22;
        int c = 33;
        variadic::partial_fold_inplace(fox::add, a, b, c);
        assert(a == 11.5);
        assert(b == 33);
        assert(c == 66);
    }
    {
        int a = 11;
        double b = 22.5;
        int c = 33;
        variadic::partial_fold_inplace(fox::add, a, b, c);
        assert(a == 11);
        assert(b == 33.5);
        assert(c == 66);
    }
    {
        int a = 11;
        int b = 22;
        double c = 33.5;
        variadic::partial_fold_inplace(fox::add, a, b, c);
        assert(a == 11);
        assert(b == 33);
        assert(c == 66.5);
    }
    {
        double a = 11.5;
        int b = 22;
        double c = 33.5;
        variadic::partial_fold_inplace(fox::add, a, b, c);
        assert(a == 11.5);
        assert(b == 33);
        assert(c == 66.5);
    }
}

void partial_fold_array()
{
    {
        int array[]{ 11, 22, 33 };
        variadic::partial_fold_inplace(fox::add, array[0], array[1], array[2]);
        assert(array[0] == 11);
        assert(array[1] == array[0] + 22);
        assert(array[2] == array[1] + 33);
    }
    {
        int array[]{ 11, 22, 33 };
        variadic::partial_fold_inplace(fox::multiply, array[0], array[1], array[2]);
        assert(array[0] == 11);
        assert(array[1] == array[0] * 22);
        assert(array[2] == array[1] * 33);
    }
    {
        int array[]{ 11, 22, 33 };
        variadic::partial_fold_inplace(fox::max, array[0], array[1], array[2]);
        assert(array[0] == 11);
        assert(array[1] == 22);
        assert(array[2] == 33);
    }
    {
        int array[]{ 33, 22, 11 };
        variadic::partial_fold_inplace(fox::min, array[0], array[1], array[2]);
        assert(array[0] == 33);
        assert(array[1] == 22);
        assert(array[2] == 11);
    }
}

void run()
{
    partial_fold_variables();
    partial_fold_mixed_variables();
    partial_fold_array();
}

} // namespace suite_partial_fold_inplace

//-----------------------------------------------------------------------------

int main()
{
    suite_partial_fold::run();
    suite_partial_fold_inplace::run();
    return 0;
}
