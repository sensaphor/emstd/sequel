///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/std/type_traits.hpp>
#include <emstd/variadic/arity.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace test
{

constexpr struct {
    template <typename T>
    constexpr void operator()(T&&) {}

    template <typename T>
    constexpr void operator()(T&&, T&&, T&&) {}
} f13;

constexpr struct {
    template <typename... Args>
    constexpr void operator()(Args&&...) {}
} fall;

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_arity_N
{

static_assert(is_same<remove_cv_t<decltype(variadic::arity_0)>, variadic::arity<0>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_1)>, variadic::arity<1>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_2)>, variadic::arity<2>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_3)>, variadic::arity<3>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_4)>, variadic::arity<4>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_5)>, variadic::arity<5>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_6)>, variadic::arity<6>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_7)>, variadic::arity<7>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_8)>, variadic::arity<8>>(), "");
static_assert(is_same<remove_cv_t<decltype(variadic::arity_9)>, variadic::arity<9>>(), "");

} // namespace suite_arity_N

//-----------------------------------------------------------------------------

namespace suite_arity_of
{

static_assert(is_same<variadic::arity_of<decltype(test::f13)>, variadic::arity<0>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::f13), int>, variadic::arity<1>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::f13), int, int>, variadic::arity<1>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::f13), int, int, int>, variadic::arity<3>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::f13), int, int, int, int>, variadic::arity<3>>(), "");

static_assert(is_same<variadic::arity_of<decltype(test::fall)>, variadic::arity<0>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), int>, variadic::arity<1>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), int, int>, variadic::arity<2>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), int, int, int>, variadic::arity<3>>(), "");

static_assert(is_same<variadic::arity_of<decltype(test::fall), variadic::arity<0>, int, int>, variadic::arity<0>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), variadic::arity<1>, int, int>, variadic::arity<1>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), variadic::arity<2>, int, int>, variadic::arity<2>>(), "");

// cv-ref

static_assert(is_same<variadic::arity_of<decltype(test::fall), variadic::arity<2>&, int, int>, variadic::arity<2>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), variadic::arity<2>&&, int, int>, variadic::arity<2>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), const variadic::arity<2>, int, int>, variadic::arity<2>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), const variadic::arity<2>&, int, int>, variadic::arity<2>>(), "");
static_assert(is_same<variadic::arity_of<decltype(test::fall), const variadic::arity<2>&&, int, int>, variadic::arity<2>>(), "");

} // namespace suite_arity_of

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
