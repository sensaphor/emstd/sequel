///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/variadic/invoke.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/fox/norm.hpp>
#include <emstd/variadic/fold.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace test
{

EMSTD_INLINE_VARIABLE struct
{
    template <typename T>
    constexpr T operator()(T t) const
    {
        return t * t;
    }
} square{};

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_invoke_index
{

void invoke_unary()
{
    static_assert(variadic::invoke_index(fox::negate, variadic::sequence<0>{}, 11, 22, 33) == -11, "");
    static_assert(variadic::invoke_index(fox::negate, variadic::sequence<1>{}, 11, 22, 33) == -22, "");
    static_assert(variadic::invoke_index(fox::negate, variadic::sequence<2>{}, 11, 22, 33) == -33, "");
}

void invoke_binary()
{
    static_assert(variadic::invoke_index(fox::add, variadic::sequence<0, 1>{}, 11, 22, 33) == 11 + 22, "");
    static_assert(variadic::invoke_index(fox::add, variadic::sequence<0, 2>{}, 11, 22, 33) == 11 + 33, "");
    static_assert(variadic::invoke_index(fox::add, variadic::sequence<1, 2>{}, 11, 22, 33) == 22 + 33, "");

    static_assert(variadic::invoke_index(fox::subtract, variadic::sequence<0, 2>{}, 11, 22, 33) == 11 - 33, "");
    static_assert(variadic::invoke_index(fox::subtract, variadic::sequence<2, 0>{}, 11, 22, 33) == 33 - 11, "");
}

} // namespace suite_invoke_index

//-----------------------------------------------------------------------------

namespace suite_invoke_over_index
{

void unary_unary()
{
    static_assert(variadic::invoke_over_index(fox::negate, test::square, variadic::arity_1, variadic::sequence<0>{}, 1, 11, 2, 22) == -1, "");
    static_assert(variadic::invoke_over_index(fox::negate, test::square, variadic::arity_1, variadic::sequence<1>{}, 1, 11, 2, 22) == -121, "");
    static_assert(variadic::invoke_over_index(fox::negate, test::square, variadic::arity_1, variadic::sequence<2>{}, 1, 11, 2, 22) == -4, "");
    static_assert(variadic::invoke_over_index(fox::negate, test::square, variadic::arity_1, variadic::sequence<3>{}, 1, 11, 2, 22) == -484, "");
}

void unary_binary()
{
    static_assert(variadic::invoke_over_index(fox::negate, fox::multiply, variadic::arity_2, variadic::sequence<0>{}, 1, 11, 2, 22) == -11, "");
    static_assert(variadic::invoke_over_index(fox::negate, fox::multiply, variadic::arity_2, variadic::sequence<2>{}, 1, 11, 2, 22) == -44, "");
    static_assert(variadic::invoke_over_index(fox::add, fox::multiply, variadic::arity_2, variadic::sequence<0, 2>{}, 1, 11, 2, 22) == 55, "");
}

void binary_unary()
{
    static_assert(variadic::invoke_over_index(fox::add, test::square, variadic::arity_1, variadic::sequence<0, 1>{}, 1, 11, 2, 22) == 1 + 121, "");
    static_assert(variadic::invoke_over_index(fox::add, test::square, variadic::arity_1, variadic::sequence<2, 3>{}, 1, 11, 2, 22) == 4 + 484, "");
}

void binary_binary()
{
    static_assert(variadic::invoke_over_index(fox::add, fox::multiply, variadic::arity_2, variadic::sequence<0, 2>{}, 1, 11, 2, 22) == 55, "");
}

} // namespace suite_invoke_over_index

//-----------------------------------------------------------------------------

namespace suite_invoke_over
{

void add_multiply()
{
    static_assert(variadic::invoke_over(fox::add, fox::multiply, 1, 11, 2, 22) == 55, "");

    constexpr auto test_add_multiply = core::bind_front(variadic::invoke_over, fox::add, fox::multiply);
    static_assert(test_add_multiply(1, 11, 2, 22) == 55, "");
}

void accumulate_multiply()
{
    constexpr auto test_accumulate = core::bind_front(variadic::fold, fox::add);
    static_assert(variadic::invoke_over(test_accumulate, fox::multiply, 1, 11) == 11, "");
    static_assert(variadic::invoke_over(test_accumulate, fox::multiply, 1, 11, 2, 22) == 55, "");
    static_assert(variadic::invoke_over(test_accumulate, fox::multiply, 1, 11, 2, 22, 3, 33) == 154, "");

    constexpr auto test_inner_product = core::bind_front(variadic::invoke_over, test_accumulate, fox::multiply);
    static_assert(test_inner_product(1, 11) == 11, "");
    static_assert(test_inner_product(1, 11, 2, 22) == 55, "");
    static_assert(test_inner_product(1, 11, 2, 22, 3, 33) == 154, "");
}

} // namespace suite_invoke_over

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
