///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/tacit/fold.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/core/dense_tuple.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_fold
{

void fold_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::fold(fox::add, array) == 11, "");
        static_assert(tacit::fold(fox::multiply, array) == 11, "");
    }
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::fold(fox::add, array) == 33, "");
        static_assert(tacit::fold(fox::multiply, array) == 242, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        static_assert(tacit::fold(fox::add, array) == 66, "");
        static_assert(tacit::fold(fox::multiply, array) == 7986, "");
    }
}

void fold_tuple()
{
    {
        constexpr core::dense_tuple<int> tuple{ 11 };
        static_assert(tacit::fold(fox::add, tuple) == 11, "");
        static_assert(tacit::fold(fox::multiply, tuple) == 11, "");
    }
    {
        constexpr core::dense_tuple<int, int> tuple{ 11, 22 };
        static_assert(tacit::fold(fox::add, tuple) == 33, "");
        static_assert(tacit::fold(fox::multiply, tuple) == 242, "");
    }
    {
        constexpr core::dense_tuple<int, int, int> tuple{ 11, 22, 33 };
        static_assert(tacit::fold(fox::add, tuple) == 66, "");
        static_assert(tacit::fold(fox::multiply, tuple) == 7986, "");
    }
    // Mixed type
    {
        constexpr core::dense_tuple<double, int, int> tuple{ 11.0, 22, 33 };
        static_assert(tacit::fold(fox::add, tuple) == 66.0, "");
    }
    {
        constexpr core::dense_tuple<int, double, int> tuple{ 11, 22.0, 33 };
        static_assert(tacit::fold(fox::add, tuple) == 66.0, "");
    }
    {
        constexpr core::dense_tuple<int, int, double> tuple{ 11, 22, 33.0 };
        static_assert(tacit::fold(fox::add, tuple) == 66.0, "");
    }
}

void fold_sequence_array()
{
    constexpr int array[]{ 11, 22, 33, 44 };

    static_assert(tacit::fold(fox::add, variadic::sequence<0>{}, array) == 11, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<1>{}, array) == 22, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<2>{}, array) == 33, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<3>{}, array) == 44, "");

    static_assert(tacit::fold(fox::add, variadic::sequence<0, 1>{}, array) == 11 + 22, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<1, 2>{}, array) == 22 + 33, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<2, 3>{}, array) == 33 + 44, "");

    static_assert(tacit::fold(fox::add, variadic::sequence<0, 1, 2>{}, array) == 11 + 22 + 33, "");
    static_assert(tacit::fold(fox::add, variadic::sequence<1, 2, 3>{}, array) == 22 + 33 + 44, "");

    static_assert(tacit::fold(fox::add, variadic::sequence<0, 1, 2, 3>{}, array) == 11 + 22 + 33 + 44, "");

    static_assert(tacit::fold(fox::add, variadic::enumerate_sequence<4>{}, array) == 11 + 22 + 33 + 44, "");

    // Reverse traversal
    static_assert(tacit::fold(fox::subtract, variadic::sequence<1, 0>{}, array) == 22 - 11, "");
    static_assert(tacit::fold(fox::subtract, variadic::sequence<2, 1>{}, array) == 33 - 22, "");
    static_assert(tacit::fold(fox::subtract, variadic::sequence<3, 2>{}, array) == 44 - 33, "");

    static_assert(tacit::fold(fox::subtract, variadic::sequence<3, 2, 1, 0>{}, array) == 44 - 33 - 22 - 11, "");
    static_assert(tacit::fold(fox::subtract, template_reverse<variadic::enumerate_sequence<4>>{}, array) == 44 - 33 - 22 - 11, "");
}

} // namespace suite_fold

//-----------------------------------------------------------------------------

namespace suite_fold_over
{

void fold_array()
{
    {
        constexpr int lhs[]{ 11 };
        constexpr int rhs[]{ 1 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 10, "");
    }
    {
        constexpr int lhs[]{ 11, 22 };
        constexpr int rhs[]{ 1, 2 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 30, "");
    }
    {
        constexpr int lhs[]{ 11, 22, 33 };
        constexpr int rhs[]{ 1, 2, 3 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 60, "");
    }
}

void fold_tuple()
{
    {
        constexpr core::dense_tuple<int> lhs{ 11 };
        constexpr core::dense_tuple<int> rhs{ 1 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 10, "");
    }
    {
        constexpr core::dense_tuple<int, int> lhs{ 11, 22 };
        constexpr core::dense_tuple<int, int> rhs{ 1, 2 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 30, "");
    }
    {
        constexpr core::dense_tuple<int, int, int> lhs{ 11, 22, 33 };
        constexpr core::dense_tuple<int, int, int> rhs{ 1, 2, 3 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 60, "");
    }
}

void fold_mixed()
{
    {
        constexpr core::dense_tuple<int, int, int> lhs{ 11, 22, 33 };
        constexpr int rhs[]{ 1, 2, 3 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 60, "");
    }
    {
        constexpr int lhs[]{ 11, 22, 33 };
        constexpr core::dense_tuple<int, int, int> rhs{ 1, 2, 3 };
        static_assert(tacit::fold_over(fox::add, fox::subtract, lhs, rhs) == 60, "");
    }
}

void fold_sequence_array()
{
    constexpr int lhs[]{ 11, 22, 33, 44 };
    constexpr int rhs[]{ 1, 2, 3, 4 };

    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<0>{}, lhs, rhs) == 10, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<1>{}, lhs, rhs) == 20, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<2>{}, lhs, rhs) == 30, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<3>{}, lhs, rhs) == 40, "");

    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<0, 1>{}, lhs, rhs) == 10 + 20, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<1, 2>{}, lhs, rhs) == 20 + 30, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<2, 3>{}, lhs, rhs) == 30 + 40, "");

    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<0, 1, 2>{}, lhs, rhs) == 10 + 20 + 30, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<1, 2, 3>{}, lhs, rhs) == 20 + 30 + 40, "");

    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::sequence<0, 1, 2, 3>{}, lhs, rhs) == 10 + 20 + 30 + 40, "");
    static_assert(tacit::fold_over(fox::add, fox::subtract, variadic::enumerate_sequence<4>{}, lhs, rhs) == 10 + 20 + 30 + 40, "");

    // Reverse traversal

    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<3, 2>{}, lhs, rhs) == 40 - 30, "");
    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<2, 1>{}, lhs, rhs) == 30 - 20, "");
    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<1, 0>{}, lhs, rhs) == 20 - 10, "");

    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<3, 2, 1>{}, lhs, rhs) == 40 - 30 - 20, "");
    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<2, 1, 0>{}, lhs, rhs) == 30 - 20 - 10, "");

    static_assert(tacit::fold_over(fox::subtract, fox::subtract, variadic::sequence<3, 2, 1, 0>{}, lhs, rhs) == 40 - 30 - 20 - 10, "");
    static_assert(tacit::fold_over(fox::subtract, fox::subtract, template_reverse<variadic::enumerate_sequence<4>>{}, lhs, rhs) == 40 - 30 - 20 - 10, "");
}

} // namespace suite_fold_over

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
