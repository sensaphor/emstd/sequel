///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/tacit/apply.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/variadic/sequence.hpp>
#include <emstd/variadic/fold.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_apply_sequence
{

void apply_sequence_add()
{
    // Front
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<0, 1>{}, fox::add, array) == 33);
    }
    // Back
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<2, 3>{}, fox::add, array) == 77);
    }
    // Even
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<0, 2>{}, fox::add, array) == 44);
    }
    // Odd
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<1, 3>{}, fox::add, array) == 66);
    }
}

void apply_sequence_subtract()
{
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<1, 0>{}, fox::subtract, array) == 11);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<2, 0>{}, fox::subtract, array) == 22);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<3, 0>{}, fox::subtract, array) == 33);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(variadic::enumerate_sequence<2>{}, fox::subtract, array) == -11);
    }
    {
        int array[]{ 11, 22, 33, 44 };
        assert(tacit::apply_sequence(template_reverse<variadic::enumerate_sequence<2>>{}, fox::subtract, array) == 11);
    }
}

void apply_sequence_inner_product()
{
    auto inner_product = core::bind_front(variadic::invoke_over, core::bind_front(variadic::fold, fox::add), fox::multiply, variadic::arity_2);
    {
        int first[]{ 11, 22 };
        int second[]{ 33, 44 };
        assert(tacit::apply_sequence(variadic::sequence<0, 1, 0, 1>{}, variadic::sequence<0, 0, 1, 1>{},
                                     inner_product,
                                     first,
                                     second) == 1331);
    }
}

void run()
{
    apply_sequence_add();
    apply_sequence_subtract();
    apply_sequence_inner_product();
}

} // namespace suite_apply_sequence

//-----------------------------------------------------------------------------

namespace suite_apply
{
void apply_add()
{
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::apply(fox::add, array) == 33, "");
    }
}

void apply_subtract()
{
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::apply(fox::subtract, array) == -11, "");
    }
}

} // namespace suite_apply

//-----------------------------------------------------------------------------

namespace suite_apply_zip
{

void apply_inner_product()
{
    constexpr auto inner_product = core::bind_front(variadic::fold_over, fox::add, fox::multiply);
    {
        constexpr int first[]{ 11, 22 };
        constexpr int second[]{ 1, 2 };
        static_assert(tacit::apply_zip(inner_product, first, second) == 11 * 1 + 22 * 2, "");
    }
    {
        constexpr int first[]{ 11, 22, 33 };
        constexpr int second[]{ 1, 2, 3 };
        static_assert(tacit::apply_zip(inner_product, first, second) == 11 * 1 + 22 * 2 + 33 * 3, "");
    }
}

} // namespace suite_apply_zip

//-----------------------------------------------------------------------------

int main()
{
    suite_apply_sequence::run();
    return 0;
}
