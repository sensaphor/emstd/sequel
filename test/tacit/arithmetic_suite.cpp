///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/tacit/arithmetic.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_add
{

void add_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::add(array) == 11, "");
    }
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::add(array) == 11 + 22, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        static_assert(tacit::add(array) == 11 + 22 + 33, "");
    }
    {
        constexpr int first[]{ 1, 2, 3 };
        constexpr int second[]{ 10, 20, 30 };
        static_assert(tacit::add(first, second) == 6 + 60, "");
    }
}

void add_bind()
{
    {
        constexpr auto add = tacit::add;
        constexpr int array[]{ 11, 22, 33 };
        static_assert(add(array) == 11 + 22 + 33, "");
    }
    {
        constexpr auto add = core::bind_front(tacit::add);
        constexpr int array[]{ 11, 22, 33 };
        static_assert(add(array) == 11 + 22 + 33, "");
    }
}

} // namespace suite_add

//-----------------------------------------------------------------------------

namespace suite_subtract
{

void subtract_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::subtract(array) == 11, "");
    }
    {
        constexpr int array[]{ 33, 22 };
        static_assert(tacit::subtract(array) == 11, "");
    }
    {
        constexpr int array[]{ 66, 33, 22 };
        static_assert(tacit::subtract(array) == 11, "");
    }
    {
        constexpr int first[]{ 30, 20, 10 };
        constexpr int second[]{ 3, 2, 1 };
        static_assert(tacit::subtract(first, second) == -6, "");
    }
}

void subtract_bind()
{
    {
        constexpr auto subtract = tacit::subtract;
        constexpr int array[]{ 66, 33, 22 };
        static_assert(subtract(array) == 66 - 33 - 22, "");
    }
    {
        constexpr auto subtract = core::bind_front(tacit::subtract);
        constexpr int array[]{ 66, 33, 22 };
        static_assert(subtract(array) == 66 - 33 - 22, "");
    }
}

} // namespace suite_subtract

//-----------------------------------------------------------------------------

namespace suite_multiply
{

void multiply_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::multiply(array) == 11, "");
    }
    {
        constexpr int array[]{ 22, 11 };
        static_assert(tacit::multiply(array) == 22 * 11, "");
    }
    {
        constexpr int array[]{ 33, 22, 11 };
        static_assert(tacit::multiply(array) == 33 * 22 * 11, "");
    }
    {
        constexpr int first[]{ 30, 20, 10 };
        constexpr int second[]{ 3, 2, 1 };
        static_assert(tacit::multiply(first, second) == 30 * 20 * 10 * 3 * 2 * 1, "");
    }
}

void multiply_bind()
{
    {
        constexpr auto multiply = tacit::multiply;
        constexpr int array[]{ 11, 22, 33 };
        static_assert(multiply(array) == 11 * 22 * 33, "");
    }
    {
        constexpr auto multiply = core::bind_front(tacit::multiply);
        constexpr int array[]{ 11, 22, 33 };
        static_assert(multiply(array) == 11 * 22 * 33, "");
    }
}

} // namespace suite_multiply

//-----------------------------------------------------------------------------

namespace suite_dot_product
{

void dot_product_array()
{
    {
        constexpr int lhs[]{ 1 };
        constexpr int rhs[]{ 10 };
        static_assert(tacit::dot_product(lhs, rhs) == 10, "");
    }
    {
        constexpr int lhs[]{ 1, 2 };
        constexpr int rhs[]{ 10, 20 };
        static_assert(tacit::dot_product(lhs, rhs) == 10 + 40, "");
    }
    {
        constexpr int lhs[]{ 1, 2, 3 };
        constexpr int rhs[]{ 10, 20, 30 };
        static_assert(tacit::dot_product(lhs, rhs) == 10 + 40 + 90, "");
    }
}

} // namespace suite_dot_product

//-----------------------------------------------------------------------------

namespace suite_partial_add
{

void partial_add_array()
{
    {
        const int array[]{ 11 };
        int result[1]{};
        tacit::partial_add(result, array);
        assert(result[0] == 11);
    }
    {
        const int array[]{ 11, 22 };
        int result[2]{};
        tacit::partial_add(result, array);
        assert(result[0] == 11);
        assert(result[1] == 11 + 22);
    }
    {
        const int array[]{ 11, 22, 33 };
        int result[3]{};
        tacit::partial_add(result, array);
        assert(result[0] == 11);
        assert(result[1] == 11 + 22);
        assert(result[2] == 11 + 22 + 33);
    }
}

void run()
{
    partial_add_array();
}

} // namespace suite_partial_add

//-----------------------------------------------------------------------------

namespace suite_partial_multiply
{

void partial_multiply_array()
{
    {
        const int array[]{ 11 };
        int result[1]{};
        tacit::partial_multiply(result, array);
        assert(result[0] == 11);
    }
    {
        const int array[]{ 11, 22 };
        int result[2]{};
        tacit::partial_multiply(result, array);
        assert(result[0] == 11);
        assert(result[1] == 11 * 22);
    }
    {
        const int array[]{ 11, 22, 33 };
        int result[3]{};
        tacit::partial_multiply(result, array);
        assert(result[0] == 11);
        assert(result[1] == 11 * 22);
        assert(result[2] == 11 * 22 * 33);
    }
}

void run()
{
    partial_multiply_array();
}

} // namespace suite_partial_multiply

//-----------------------------------------------------------------------------

int main()
{
    suite_partial_add::run();
    suite_partial_multiply::run();
    return 0;
}
