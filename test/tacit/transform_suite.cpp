///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/tacit/transform.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/variadic/fold.hpp>
#include <emstd/variadic/arithmetic.hpp>
#include <emstd/array.hpp>
#include <emstd/core/dense_tuple.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_transform
{

void transform_unary()
{
    {
        const int array[]{ 11 };
        int result[1]{};
        tacit::transform(fox::negate, result, array);
        assert(result[0] == -11);
    }
    {
        const int array[]{ 11, 22 };
        int result[2]{};
        tacit::transform(fox::negate, result, array);
        assert(result[0] == -11);
        assert(result[1] == -22);
    }
    {
        const int array[]{ 11, 22, 33 };
        int result[3]{};
        tacit::transform(fox::negate, result, array);
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
    }
    {
        const int array[]{ 11, 22, 33, 44 };
        int result[4]{};
        tacit::transform(fox::negate, result, array);
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
    {
        const int first[]{ 11, 22 };
        const int second[]{ 33, 44 };
        int result[4]{};
        tacit::transform(fox::negate, result, first, second);
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
    // Rvalue
    {
        int result[4]{};
        tacit::transform(fox::negate, result, emstd::array<int, 4>{ 11, 22, 33, 44});
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
    {
        int result[4]{};
        tacit::transform(fox::negate, result, core::dense_tuple<int, int, int, int>{ 11, 22, 33, 44});
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
    // Inplace
    {
        int array[]{ 11, 22, 33, 44 };
        tacit::transform(fox::negate, array, array);
        assert(array[0] == -11);
        assert(array[1] == -22);
        assert(array[2] == -33);
        assert(array[3] == -44);
    }
    // Mixed types
    {
        const core::dense_tuple<int, int, int, int> tuple{ 11, 22, 33, 44 };
        int result[4]{};
        tacit::transform(fox::negate, result, tuple);
        assert(result[0] == -11);
        assert(result[1] == -22);
        assert(result[2] == -33);
        assert(result[3] == -44);
    }
    {
        const int array[]{ 11, 22, 33, 44 };
        core::dense_tuple<int, int, int, int> result{};
        tacit::transform(fox::negate, result, array);
        assert(result.get<0>() == -11);
        assert(result.get<1>() == -22);
        assert(result.get<2>() == -33);
        assert(result.get<3>() == -44);
    }
}

void transform_binary()
{
    {
        const int first[]{ 11, 22, 33, 44 };
        int result[2]{};
        tacit::transform(fox::add, result, first);
        assert(result[0] == 33);
        assert(result[1] == 77);
    }
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        int result[4]{};
        tacit::transform(fox::add, result, first, second);
        assert(result[0] == 33);
        assert(result[1] == 77);
        assert(result[2] == 3);
        assert(result[3] == 7);
    }
}

void transform_nary()
{
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        int result[4]{};
        tacit::transform(variadic::add, variadic::arity_2, result, first, second);
        assert(result[0] == 33);
        assert(result[1] == 77);
        assert(result[2] == 3);
        assert(result[3] == 7);
    }
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        const int third[]{ 100, 200, 300, 400 };
        int result[4]{};
        tacit::transform(variadic::add, variadic::arity_3, result, first, second, third);
        assert(result[0] == 11 + 22 + 33);
        assert(result[1] == 44 + 1 + 2);
        assert(result[2] == 3 + 4 + 100);
        assert(result[3] == 200 + 300 + 400);
    }
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        const int third[]{ 100, 200, 300, 400 };
        const int fourth[]{ 1000, 2000, 3000, 4000 };
        int result[4]{};
        tacit::transform(variadic::add, variadic::arity_4, result, first, second, third, fourth);
        assert(result[0] == 11 + 22 + 33 + 44);
        assert(result[1] == 1 + 2 + 3 + 4);
        assert(result[2] == 100 + 200 + 300 + 400);
        assert(result[3] == 1000 + 2000 + 3000 + 4000);
    }
}

void transform_bind()
{
    constexpr auto linear_accumulate = core::bind_front(tacit::transform, variadic::add);
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        int result[4]{};
        linear_accumulate(variadic::arity_2, result, first, second);
        assert(result[0] == 33);
        assert(result[1] == 77);
        assert(result[2] == 3);
        assert(result[3] == 7);
    }
    {
        const int first[]{ 11, 22, 33, 44 };
        const int second[]{ 1, 2, 3, 4 };
        const int third[]{ 100, 200, 300, 400 };
        int result[4]{};
        linear_accumulate(variadic::arity_3, result, first, second, third);
        assert(result[0] == 11 + 22 + 33);
        assert(result[1] == 44 + 1 + 2);
        assert(result[2] == 3 + 4 + 100);
        assert(result[3] == 200 + 300 + 400);
    }
}

void run()
{
    transform_unary();
    transform_binary();
    transform_nary();
    transform_bind();
}

} // namespace suite_transform

//-----------------------------------------------------------------------------

namespace suite_transform_zip
{

void transform_zip_binary()
{
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        int result[4]{};
        tacit::transform_zip(fox::add, result, first, second);
        assert(result[0] == 1 + 10);
        assert(result[1] == 2 + 20);
        assert(result[2] == 3 + 30);
        assert(result[3] == 4 + 40);
    }
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        int result[4]{};
        tacit::transform_zip(fox::multiply, result, first, second);
        assert(result[0] == 1 * 10);
        assert(result[1] == 2 * 20);
        assert(result[2] == 3 * 30);
        assert(result[3] == 4 * 40);
    }
}

void transform_zip_nary()
{
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        int result[4]{};
        tacit::transform_zip(variadic::add, variadic::arity_2, result, first, second);
        assert(result[0] == 1 + 10);
        assert(result[1] == 2 + 20);
        assert(result[2] == 3 + 30);
        assert(result[3] == 4 + 40);
    }
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        const int third[]{ 100, 200, 300, 400 };
        int result[4]{};
        tacit::transform_zip(variadic::add, variadic::arity_3, result, first, second, third);
        assert(result[0] == 1 + 10 + 100);
        assert(result[1] == 2 + 20 + 200);
        assert(result[2] == 3 + 30 + 300);
        assert(result[3] == 4 + 40 + 400);
    }
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        const int third[]{ 100, 200, 300, 400 };
        const int fourth[]{ 1000, 2000, 3000, 4000 };
        int result[4]{};
        tacit::transform_zip(variadic::add, variadic::arity_4, result, first, second, third, fourth);
        assert(result[0] == 1 + 10 + 100 + 1000);
        assert(result[1] == 2 + 20 + 200 + 2000);
        assert(result[2] == 3 + 30 + 300 + 3000);
        assert(result[3] == 4 + 40 + 400 + 4000);
    }
}

void transform_bind()
{
    constexpr auto transposed_accumulate = core::bind_front(tacit::transform_zip, variadic::add);
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        int result[4]{};
        transposed_accumulate(variadic::arity_2, result, first, second);
        assert(result[0] == 1 + 10);
        assert(result[1] == 2 + 20);
        assert(result[2] == 3 + 30);
        assert(result[3] == 4 + 40);
    }
    {
        const int first[]{ 1, 2, 3, 4 };
        const int second[]{ 10, 20, 30, 40 };
        const int third[]{ 100, 200, 300, 400 };
        int result[4]{};
        transposed_accumulate(variadic::arity_3, result, first, second, third);
        assert(result[0] == 1 + 10 + 100);
        assert(result[1] == 2 + 20 + 200);
        assert(result[2] == 3 + 30 + 300);
        assert(result[3] == 4 + 40 + 400);
    }
}

void run()
{
    transform_zip_binary();
    transform_zip_nary();
    transform_bind();
}

} // namespace suite_transform_zip

//-----------------------------------------------------------------------------

int main()
{
    suite_transform::run();
    suite_transform_zip::run();
    return 0;
}
