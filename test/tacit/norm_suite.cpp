///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/tacit/norm.hpp>

using namespace emstd;

//-----------------------------------------------------------------------------

namespace suite_max
{

void max_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::max(array) == 11, "");
    }
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::max(array) == 22, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        static_assert(tacit::max(array) == 33, "");
    }
    {
        constexpr int first[]{ 11, 22, 33 };
        constexpr int second[]{ 44, 55, 66 };
        static_assert(tacit::max(first, second) == 66, "");
    }
}

void max_bind()
{
    {
        constexpr auto max = tacit::max;
        constexpr int array[]{ 11, 22, 33 };
        static_assert(max(array) == 33, "");
    }
    {
        constexpr auto max = core::bind_front(tacit::max);
        constexpr int array[]{ 11, 22, 33 };
        static_assert(max(array) == 33, "");
    }
}

} // namespace suite_max

//-----------------------------------------------------------------------------

namespace suite_partial_max
{

void partial_max_array()
{
    {
        const int array[]{ 11 };
        int result[1]{};
        tacit::partial_max(result, array);
        assert(result[0] == 11);
    }
    {
        const int array[]{ 11, 22 };
        int result[2]{};
        tacit::partial_max(result, array);
        assert(result[0] == 11);
        assert(result[1] == 22);
    }
    {
        const int array[]{ 11, 22, 33 };
        int result[3]{};
        tacit::partial_max(result, array);
        assert(result[0] == 11);
        assert(result[1] == 22);
        assert(result[2] == 33);
    }
}

void run()
{
    partial_max_array();
}

} // namespace suite_partial_max

//-----------------------------------------------------------------------------

namespace suite_min
{

void min_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::min(array) == 11, "");
    }
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::min(array) == 11, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        static_assert(tacit::min(array) == 11, "");
    }
    {
        constexpr int first[]{ 66, 55, 44 };
        constexpr int second[]{ 33, 22, 11 };
        static_assert(tacit::min(first, second) == 11, "");
    }
}

void min_bind()
{
    {
        constexpr auto min = tacit::min;
        constexpr int array[]{ 11, 22, 33 };
        static_assert(min(array) == 11, "");
    }
    {
        constexpr auto min = core::bind_front(tacit::min);
        constexpr int array[]{ 11, 22, 33 };
        static_assert(min(array) == 11, "");
    }
}

} // namespace suite_min

//-----------------------------------------------------------------------------

namespace suite_partial_min
{

void partial_min_array()
{
    {
        const int array[]{ 33 };
        int result[1]{};
        tacit::partial_min(result, array);
        assert(result[0] == 33);
    }
    {
        const int array[]{ 33, 22 };
        int result[2]{};
        tacit::partial_min(result, array);
        assert(result[0] == 33);
        assert(result[1] == 22);
    }
    {
        const int array[]{ 33, 22, 11 };
        int result[3]{};
        tacit::partial_min(result, array);
        assert(result[0] == 33);
        assert(result[1] == 22);
        assert(result[2] == 11);
    }
}

void run()
{
    partial_min_array();
}

} // namespace suite_partial_min

//-----------------------------------------------------------------------------

namespace suite_minmax
{

void minmax_array()
{
    {
        constexpr int array[]{ 11 };
        static_assert(tacit::minmax(array).min() == 11, "");
        static_assert(tacit::minmax(array).max() == 11, "");
    }
    {
        constexpr int array[]{ 11, 22 };
        static_assert(tacit::minmax(array).min() == 11, "");
        static_assert(tacit::minmax(array).max() == 22, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        static_assert(tacit::minmax(array).min() == 11, "");
        static_assert(tacit::minmax(array).max() == 33, "");
    }
    {
        constexpr int array[]{ 11, 22, 33 };
        constexpr auto result = tacit::minmax(array);
        static_assert(result.min() == 11, "");
        static_assert(result.max() == 33, "");
    }
    {
        constexpr int first[]{ 33, 22, 11 };
        constexpr int second[]{ 66, 55, 44 };
        constexpr auto result = tacit::minmax(first, second);
        static_assert(result.min() == 11, "");
        static_assert(result.max() == 66, "");
    }
}

void minmax_bind()
{
    {
        constexpr auto minmax = tacit::minmax;
        constexpr int array[]{ 11, 22, 33 };
        static_assert(minmax(array).min() == 11, "");
        static_assert(minmax(array).max() == 33, "");
    }
    {
        constexpr auto minmax = core::bind_front(tacit::minmax);
        constexpr int array[]{ 11, 22, 33 };
        static_assert(minmax(array).min() == 11, "");
        static_assert(minmax(array).max() == 33, "");
    }
}

} // namespace suite_minmax

//-----------------------------------------------------------------------------

int main()
{
    suite_partial_max::run();
    suite_partial_min::run();
    return 0;
}
