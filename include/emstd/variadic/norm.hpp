///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_NORM_HPP
#define EMSTD_VARIADIC_NORM_HPP

#include <emstd/detail/config.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/norm.hpp>
#include <emstd/variadic/fold.hpp>
#include <emstd/variadic/partial_fold.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// Maximum value of parameter pack.
//
// Examples:
//
//   variadic::max(33, 22, 11) == 33

EMSTD_INLINE_VARIABLE auto max = core::bind_front(fold, fox::max);

//-----------------------------------------------------------------------------
// Cumulative maximum value of parameter pack.
//
// Examples:
//
//   variadic::partial_max(ra, rb, rc, a, b, c) becomes
//
//   ra = a
//   rb = max(ra, b)
//   rc = max(rb, c)

EMSTD_INLINE_VARIABLE auto partial_max = core::bind_front(partial_fold, fox::max);

//-----------------------------------------------------------------------------
// Minimum value of parameter pack.
//
// Examples:
//
//   variadic::min(33, 22, 11) == 11

EMSTD_INLINE_VARIABLE auto min = core::bind_front(fold, fox::min);

//-----------------------------------------------------------------------------
// Cumulative minimum value of parameter pack.
//
// Examples:
//
//   variadic::partial_min(ra, rb, rc, a, b, c) becomes
//
//   ra = a
//   rb = min(ra, b)
//   rc = min(rb, c)

EMSTD_INLINE_VARIABLE auto partial_min = core::bind_front(partial_fold, fox::min);

//-----------------------------------------------------------------------------
// Minimum and maximum value of parameter pack.
//
// Remarks:
// - Traverses the parameter back once.
//
// Examples:
//
//   variadic::minmax(33, 22, 11).min() == 11
//   variadic::minmax(33, 22, 11).max() == 11

EMSTD_INLINE_VARIABLE auto minmax = core::bind_front(fold, fox::minmax);

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_NORM_HPP
