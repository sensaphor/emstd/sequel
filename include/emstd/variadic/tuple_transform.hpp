///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_TUPLE_TRANSFORM_HPP
#define EMSTD_VARIADIC_TUPLE_TRANSFORM_HPP

#include <emstd/detail/config.hpp>
#include <emstd/detail/forward.hpp>
#include <emstd/core/void.hpp>
#include <emstd/core/enumerate_integer_sequence.hpp>
#include <emstd/core/arity.hpp>
#include <emstd/core/tuple_apply.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// Applies function to each selected tuple slice.
//
// The tuple elements are split into slices whose size equals the function
// arity. The function is applied to each selected slice.
//
// The Input parameter pack contains the start of each selected slice.
//
// The optional Output parameter pack specifies where the results are assigned
// to. If omitted, the Input parameter pack is used instead, in which case the
// results are assigned to the front element of each selected slice.
//
// The function arity may be omitted for unary transforms.
//
// Example: Unary transform of two selected slices.
//
//    tuple t{a, b, c};
//    tuple_transform_index(f1, index_sequence<0, 2>{}, t);
//
//    becomes
//
//    a = f1(a);
//    c = f1(c);
//
// Example: Binary transform of two selected slices.
//
//   tuple t{a0, b0, a1, b1, a2, b2};
//   tuple_transform_index(f2, arity_2, index_sequence<0, 4>{}, t);
//
//   becomes
//
//   a0 = f2(a0, b0);
//   a2 = f2(a2, b2);
//
// Example: As above but with results assigned to back element in slice.
//
//   tuple t{a0, b0, a1, b1, a2, b2};
//   tuple_transform_index(f2, arity_2, index_sequence<0, 4>{}, index_sequence<1, 5>{}, t);
//
//   becomes
//
//   b0 = f2(a0, b0);
//   b2 = f2(a2, b2);

EMSTD_INLINE_VARIABLE struct
{
    template <typename F, size_t Width, size_t... Input, size_t... Output, typename T>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn,
                                core::arity<Width>,
                                core::index_sequence<Input...>,
                                core::index_sequence<Output...>,
                                T&& tuple) const
    {
        return supervoid{
            ext::get<Output>(tuple) =
                core::tuple_apply_index(forward_cast<F>(fn),
                                        core::enumerate_index_sequence<Width, Input, 1>{},
                                        forward_cast<T>(tuple))...
        };
    }

    template <typename F, size_t Width, size_t... Input, typename T>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<Width> width, core::index_sequence<Input...> seq, T&& tuple) const
    {
        return (*this)(forward_cast<F>(fn),
                       width,
                       seq,
                       seq,
                       forward_cast<T>(tuple));
    }

    // Mandates:
    // - Unary function.

    template <typename F, size_t... Input, typename T>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::index_sequence<Input...> seq, T&& tuple) const
    {
        static_assert(is_invocable<F, core::tuple_element_t<0, remove_cvref_t<T>>>(), "Specify arity");

        return (*this)(forward_cast<F>(fn),
                       core::arity_1,
                       seq,
                       forward_cast<T>(tuple));
    }
} tuple_transform_index{};

//-----------------------------------------------------------------------------
// Applies function to all tuple slices.
//
// The tuple elements are split into slices whose size equals the function
// arity. The function is applied to each slice.
//
// Mandates:
// - Tuple size is a multiple of the function arity.

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              size_t Width,
              typename T,
              size_t Size = core::tuple_size<remove_cvref_t<T>>::value>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<Width> width, T&& tuple) const
    {
        static_assert((Size % Width) == 0, "Tuple size must be a multiple of the function arity");

        return tuple_transform_index(forward_cast<F>(fn),
                                     width,
                                     core::enumerate_index_sequence<Size / Width, 0, Width>{},
                                     forward_cast<T>(tuple));
    }

    // Mandates:
    // - Unary function

    template <typename F, typename T>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, T&& tuple) const
    {
        static_assert(is_invocable<F, core::tuple_element_t<0, remove_cvref_t<T>>>(), "Specify arity");

        return (*this)(forward_cast<F>(fn),
                       core::arity_1,
                       forward_cast<T>(tuple));
    }
} tuple_transform{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_TUPLE_TRANSFORM_HPP
