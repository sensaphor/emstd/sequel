///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_ALGORITHM_HPP
#define EMSTD_VARIADIC_ALGORITHM_HPP

#include <emstd/fox/logical.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/variadic/invoke.hpp>
#include <emstd/variadic/fold.hpp>

namespace emstd
{
namespace variadic
{

EMSTD_INLINE_VARIABLE auto logical_and = core::bind_front(fold, fox::logical_and);
EMSTD_INLINE_VARIABLE auto logical_or = core::bind_front(fold, fox::logical_or);
EMSTD_INLINE_VARIABLE auto logical_none = core::bind_front(invoke_over, fox::logical_not, logical_or);

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_ALGORITHM_HPP
