///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_PARTIAL_FOLD_HPP
#define EMSTD_VARIADIC_PARTIAL_FOLD_HPP

#include <emstd/detail/config.hpp>
#include <emstd/detail/forward.hpp>
#include <emstd/core/variadic_traits.hpp>
#include <emstd/fox/invoke.hpp>
#include <emstd/core/void.hpp>
#include <emstd/std/tuple.hpp>
#include <emstd/core/tuplean.hpp>
#include <emstd/variadic/sequence.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// partial_fold
//
// Cumulative left fold of F over variadic arguments.
//
// Mandates:
// - Even number of arguments.
// - The first half of the arguments are mutable lvalue references.
//
// Examples:
//
//   variadic::partial_fold(f2, ra, rb, rc, a, b, c) becomes
//
//   ra = a
//   rb = f2(ra, b)
//   rc = f2(rb, c)

EMSTD_INLINE_VARIABLE struct
{
private:
    template <typename F,
              typename Tuple,
              size_t... Index,
              size_t Offset = 1 + sizeof...(Index)>
    static constexpr voider invoke(F&& fn, Tuple&& tuple, sequence<Index...>)
    {
        static_assert(variadic_and<is_mutable_lvalue_reference<decltype(tuple.template get<0>())>,
                                   is_mutable_lvalue_reference<decltype(tuple.template get<Index>())>...>::value,
                      "Result argument must be a mutable lvalue reference");

        // Assign left-to-right as a side-effect of initializing a temporary array
        return supervoid{
            { tuple.template get<0>() = tuple.template get<Offset>() },
            { tuple.template get<Index>() = fox::invoke(fn, tuple.template get<Index - 1>(), tuple.template get<Index + Offset>()) }... };
    }

public:
    template <typename F, typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, Args&&... args) const
    {
        static_assert((sizeof...(Args) % 2) == 0, "There must be an even number of arguments");

        return invoke(forward_cast<F>(fn),
                      core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...),
                      // Argument 0 is handled seperately
                      enumerate_sequence<sizeof...(Args) / 2 - 1, 1>{});
    }
} partial_fold{};

//-----------------------------------------------------------------------------
// inplace_partial_fold
//
// Mandates:
// - Each argument is a mutable lvalue reference.

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              typename... Args>
    constexpr voider operator()(F&& fn, Args&... args) const
    {
        return partial_fold(forward_cast<F>(fn), args..., args...);
    }
} partial_fold_inplace{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_PARTIAL_FOLD_HPP
