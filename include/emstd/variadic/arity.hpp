///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_ARITY_HPP
#define EMSTD_VARIADIC_ARITY_HPP

#include <emstd/core/type_traits.hpp>
#include <emstd/core/arity.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// arity

using core::arity;

using core::arity_0;
using core::arity_1;
using core::arity_2;
using core::arity_3;
using core::arity_4;
using core::arity_5;
using core::arity_6;
using core::arity_7;
using core::arity_8;
using core::arity_9;

using core::is_arity;

//-----------------------------------------------------------------------------
// arity_of
//
// Determines the largest arity of given function and arguments.
//
// Remarks:
// - Arity is 0 if no valid invocation is found.
//
// Example: Function with two overloads
//
//   void f(A);
//   void f(A, B, C);
//
//   arity<decltype(f), A, B, C, D> becomes
//   arity<3>
//
//   arity<decltype(f), arity<1>, A, B, C, D> becomes
//   arity<1>

namespace detail
{

template <size_t, typename, typename, typename>
struct arity_greedy;

// The steps of the example above is
//
//   f(A) is invocable              => Arity = 1
//   f(A, B) is not invocable       => Arity = 1 (last valid arity)
//   f(A, B, C) is invocable        => Arity = 3
//   f(A, B, C, D) is not invocable => Arity = 3 (last valid arity)

template <size_t Arity, typename F, typename... Head, typename T, typename... Tail>
struct arity_greedy<Arity,
                    F,
                    prototype<Head...>,
                    prototype<T, Tail...>>
    : arity_greedy<is_invocable<F, Head..., T>::value ? sizeof...(Head) + 1 : Arity,
                   F, prototype<Head..., T>, prototype<Tail...>>
{
};

template <size_t Arity, typename F, typename... Head>
struct arity_greedy<Arity, F, prototype<Head...>, prototype<>>
{
    using type = arity<Arity>;
};

// Special case: First argument is (cv-ref) arity<N>

template <typename F, typename A, typename = void>
struct arity_preset
    : arity_greedy<0, F, prototype<>, A>
{
};

template <typename F, typename Arg0, typename... Args>
struct arity_preset<F, prototype<Arg0, Args...>, constrains_if<is_arity<remove_cvref_t<Arg0>>>>
{
    using type = remove_cvref_t<Arg0>;
};

} // namespace detail

template <typename F, typename... Args>
using arity_of = type_t<detail::arity_preset<decay_t<F>, prototype<Args...>>>;

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_ARITY_HPP
