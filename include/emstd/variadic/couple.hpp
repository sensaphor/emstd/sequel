///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_COUPLE_HPP
#define EMSTD_VARIADIC_COUPLE_HPP

#include <emstd/std/type_traits.hpp>

namespace emstd
{
namespace variadic
{

// Single-typed pair

template <typename T>
struct couple
{
    template <constrains_if<std::is_default_constructible<T>>* = nullptr>
    constexpr couple() {}

    constexpr couple(const couple&) = default;

    constexpr couple(T value) : first(value), second(value) {}
    constexpr couple(T first, T second) : first(first), second(second) {}

    T first;
    T second;

private:
    friend constexpr bool operator==(const couple& lhs, const couple& rhs)
    {
        return (lhs.first == rhs.first) && (lhs.second == rhs.second);
    }

    friend constexpr bool operator!=(const couple& lhs, const couple& rhs)
    {
        return !(lhs == rhs);
    }
};

template <typename T>
constexpr auto make_couple(T lhs, T rhs)
    -> couple<T>
{
    return { lhs, rhs };
}

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_COUPLE_HPP
