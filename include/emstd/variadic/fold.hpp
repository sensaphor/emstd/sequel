///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_FOLD_HPP
#define EMSTD_VARIADIC_FOLD_HPP

#include <emstd/detail/config.hpp>
#include <emstd/detail/forward.hpp>
#include <emstd/type_traits.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/core/fold.hpp>
#include <emstd/variadic/sequence.hpp>
#include <emstd/variadic/invoke.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// fold
//
// Left fold of F over variadic arguments.
//
// Examples:
//
//   variadic::fold(f2, a, b, c, d) becomes
//   f2(f2(f2(a, b), c), d)

using core::fold;

//-----------------------------------------------------------------------------
// fold_over
//
// Applies second function (projection) to each variadic group and folds the
// results with the first function.
//
// An optional function arity for the second function may be specified
// immediately after the functions. If omitted, the arity is deduced from the
// projection function.
//
// The projection arguments are consumed left-to-right.
//
// Examples: Fold with unary projection
//
//   variadic::fold_over(f, g, a, b, c) becomes
//
//   variadic::fold(f, g(a), g(b), g(c))
//
// Examples: Fold with binary projection
//
//   variadic::fold_over(f, g2, a1, a2, b1, b2, c1, c2) or
//   variadic::fold_over(f, g2, arity<2>{} a1, a2, b1, b2, c1, c2) becomes
//
//   variadic::fold(f, g2(a1,a2), g2(b1, b2), g2(c1, c2))

EMSTD_INLINE_VARIABLE struct
{
    template <typename Fold,
              typename Project,
              typename... Args>
    constexpr auto operator()(const Fold& fn, const Project& proj, Args&&... args) const
        -> decltype(invoke_over(core::bind_front(declare<decltype(fold)>(), declare<Fold>()),
                                declare<Project>(),
                                declare<Args>()...))
    {
        return invoke_over(core::bind_front(fold, fn),
                           proj,
                           forward_cast<Args>(args)...);
    }

} fold_over{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_FOLD_HPP
