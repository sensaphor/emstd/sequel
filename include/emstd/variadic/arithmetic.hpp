///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_ARITHMETIC_HPP
#define EMSTD_VARIADIC_ARITHMETIC_HPP

#include <emstd/detail/config.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/fox/arithmetic.hpp>
#include <emstd/variadic/fold.hpp>
#include <emstd/variadic/partial_fold.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// Addition (sum) of parameter pack.
//
// Example:
//
//   variadic::add(a, b, c) == a + b + c

EMSTD_INLINE_VARIABLE auto add = core::bind_front(fold, fox::add);

//-----------------------------------------------------------------------------
// Subtraction of parameter pack.
//
// The first argument is the minuend and the rest are the subtrahends.
//
// Example:
//
//   variadic::subtract(a, b, c) == a - b - c

EMSTD_INLINE_VARIABLE auto subtract = core::bind_front(fold, fox::subtract);

//-----------------------------------------------------------------------------
// Multiplication (product) of parameter pack.
//
// The first argument is the multiplier and the rest are the multiplicands.
//
// Examples:
//
//   variadic::multiply(a, b, c) == a * b * c

EMSTD_INLINE_VARIABLE auto multiply = core::bind_front(fold, fox::multiply);

//-----------------------------------------------------------------------------
// Projected addition
//
// Example: Unary projection
//
//   variadic::add_over(fox::abs, a, b, c) == abs(a) + abs(b) + abs(c)
//
// Example: Binary projection
//
//   variadic::add_over(fox::multiply, a, b, c, d) == a * b + c * d

EMSTD_INLINE_VARIABLE auto add_over = core::bind_front(fold_over, fox::add);

//-----------------------------------------------------------------------------
// Projected subtraction
//
// Example: Unary projection
//
//   variadic::subtract_over(fox::abs, a, b, c) == abs(a) - abs(b) - abs(c)
//
// Example: Binary projection
//
//   variadic::subtract_over(fox::multiply, a, b, c, d) == a * b - c * d

EMSTD_INLINE_VARIABLE auto subtract_over = core::bind_front(fold_over, fox::subtract);

//-----------------------------------------------------------------------------
// Projected multiplication
//
// Example: Unary projection
//
//   variadic::multiply_over(fox::abs, a, b, c) == abs(a) * abs(b) * abs(c)
//
// Example: Binary projection
//
//   variadic::multiply_over(fox::subtract, a, b, c, d) == (a - b) * (c - d)

EMSTD_INLINE_VARIABLE auto multiply_over = core::bind_front(fold_over, fox::multiply);

//-----------------------------------------------------------------------------
// Dot product of parameter pack.
//
// Mandates:
// - Even number of arguments.
//
// Examples:
//
//    variadic::dot_product(a, b, c, d) == a * b + c * d

EMSTD_INLINE_VARIABLE auto dot_product = core::bind_front(fold_over, fox::add, fox::multiply);

//-----------------------------------------------------------------------------
// Cumulative addition (partial sum) of parameter pack.
//
// Examples:
//
//   variadic::partial_add(ra, rb, rc, a, b, c) becomes
//
//   ra = a
//   rb = ra + b
//   rc = rb + c

EMSTD_INLINE_VARIABLE auto partial_add = core::bind_front(partial_fold, fox::add);

//-----------------------------------------------------------------------------
// Cumulative multiplication (partial product) of parameter pack.
//
// Examples:
//
//   variadic::partial_multiply(ra, rb, rc, a, b, c) becomes
//
//   ra = a
//   rb = ra * b
//   rc = rb * c

EMSTD_INLINE_VARIABLE auto partial_multiply = core::bind_front(partial_fold, fox::multiply);

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_ARITHMETIC_HPP
