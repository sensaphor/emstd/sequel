///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_SEQUENCE_HPP
#define EMSTD_VARIADIC_SEQUENCE_HPP

#include <emstd/core/variadic_traits.hpp>
#include <emstd/core/integer_sequence.hpp>
#include <emstd/core/enumerate_integer_sequence.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// A selection of integers.

template <size_t... Index>
using sequence = core::index_sequence<Index...>;

//-----------------------------------------------------------------------------

template <typename>
struct sequence_size
    : index_constant<0>
{
};

template <template <typename...> class Tuple, typename... Types>
struct sequence_size<Tuple<Types...>>
    : index_constant<sizeof...(Types)>
{
};

template <template <typename, size_t> class Array, typename T, size_t N>
struct sequence_size<Array<T, N>>
    : index_constant<N>
{
};

template <typename T, size_t N>
struct sequence_size<T[N]>
    : index_constant<N>
{
};

//-----------------------------------------------------------------------------
// Generates enumerated sequence.
//
// One to three arguments may be used.
//
// enumerate<...> is a placeholder for an unexpanded enumerated sequence.
//
// enumerate_sequence<...> converts the enumeration to a sequence.
//
// One argument N generates the sequence from 0 to N-1.
//
//   enumerate_sequence<N> becomes sequence<0, 1, ..., N-1>
//
// Two arguments N and Offset generates the sequence from Offset to Offset + N-1.
//
//   enumerate_sequence<N, A> becomes sequence<A, A+1, ..., A+N-1>
//
// Three arguments N, Offset, and Step generates the sequence from Offset to
// Offset + Step * (N-1).
//
//   enumerate_sequence<N, A, B> becomes sequence<A, A+B, A+B*2, ..., A+B*(N-1)>

namespace detail
{

template <size_t...>
struct enumerate_sequence_helper;

template <size_t N>
struct enumerate_sequence_helper<N>
{
    using type = core::make_index_sequence<N>;
};

template <size_t N, size_t Offset>
struct enumerate_sequence_helper<N, Offset>
{
    using type = core::enumerate_index_sequence<N, Offset, 1>;
};

template <size_t N, size_t Offset, size_t Step>
struct enumerate_sequence_helper<N, Offset, Step>
{
    using type = core::enumerate_index_sequence<N, Offset, Step>;
};

} // namespace detail

template <size_t... V>
struct enumerate
{
    using type = type_t<detail::enumerate_sequence_helper<V...>>;
};

template <size_t... V>
using enumerate_sequence = type_t<enumerate<V...>>;

//-----------------------------------------------------------------------------
// Generates incremental sequence for array or tuple.

namespace detail
{

template <typename>
struct sequence_of_helper;

template <template <typename...> class Tuple, typename... T>
struct sequence_of_helper<Tuple<T...>>
{
    using type = enumerate_sequence<sizeof...(T)>;
};

template <template <typename, size_t> class Array, typename T, size_t N>
struct sequence_of_helper<Array<T, N>>
{
    using type = enumerate_sequence<N>;
};

template <typename T, size_t N>
struct sequence_of_helper<T[N]>
{
    using type = enumerate_sequence<N>;
};

} // namespace detail

template <typename T>
using sequence_of = type_t<detail::sequence_of_helper<T>>;

//-----------------------------------------------------------------------------
// Concatenates sequences with incremented elements.
//
// The first sequence contains the increments for each subsequent sequences.
// The first sequence is not concatenated with the rest.
//
// Example:
//
//   adjoin_sequence_each<index_sequence<0, 2>, index_sequence<0, 1>, index_sequence<0, 1>>
//   becomes sequence<0, 1, 2, 3>

namespace detail
{

template <typename U, U, typename>
struct adjoin_sequence_projection;

template <typename U, U Offset,
          template <typename T, T...> class Sequence, typename T,
          T... Index>
struct adjoin_sequence_projection<U, Offset, Sequence<T, Index...>>
{
    using type = Sequence<T, (Offset + Index)...>;
};
          
} // namespace detail

template <typename, typename...>
struct adjoin_sequence_each_trait;

template <template <typename T, T...> class Sequence,
          typename T, T... Offset,
          typename... Sequences>
struct adjoin_sequence_each_trait<Sequence<T, Offset...>, Sequences...>
{
    using type = template_cat<type_t<detail::adjoin_sequence_projection<T, Offset, Sequences>>...>;
};

template <typename... Types>
using adjoin_sequence_each = type_t<adjoin_sequence_each_trait<Types...>>;

//-----------------------------------------------------------------------------
// Concatenates sequences with incremented elements.
//
// Example:
//
//   adjoin_sequence<index_sequence<0, 0>, index_sequence<0, 0>>
//   becomes sequence<0, 0, 1, 1>

template <typename...>
struct adjoin_sequence_trait;

template <template <typename T, T...> class Sequence, typename T, T... Index, typename... Tail>
struct adjoin_sequence_trait<Sequence<T, Index...>, Tail...>
    : adjoin_sequence_each_trait<core::make_integer_sequence<T, 1 + sizeof...(Tail)>, Sequence<T, Index...>, Tail...>
{
};

template <typename... Types>
using adjoin_sequence = type_t<adjoin_sequence_trait<Types...>>;

//-----------------------------------------------------------------------------
// Generates row indices for an N x M type matrix.
//
// Accessing elements in a type matrix is done by generating a row sequence
// and an associated column sequence. The type matrix elements can be traversed
// via pair-wise pack expansion of the row and column sequences.

template <size_t Rows, size_t Columns>
using sequence_row = template_repeat<Columns, enumerate_sequence<Rows>>;

//-----------------------------------------------------------------------------
// Generates column indices for an N x M type matrix.
//
// The columns are specified as an integer sequence.
//
// See also sequence_row.

template <size_t, typename>
struct sequence_column_trait;

template <size_t Rows, size_t... Columns>
struct sequence_column_trait<Rows, sequence<Columns...>>
{
private:
    template <typename>
    struct element;

    template <size_t... Column>
    struct element<sequence<Column...>>
    {
        using type = template_cat<enumerate_sequence<Rows, Column, 0>...>;
    };

public:
    using type = type_t<element<sequence<Columns...>>>;
};

template <size_t Row, size_t Columns>
using sequence_column = type_t<sequence_column_trait<Row, enumerate_sequence<Columns>>>;

template <size_t Row, typename ColumnSequence>
using sequence_column_select = type_t<sequence_column_trait<Row, ColumnSequence>>;

//-----------------------------------------------------------------------------
// Generates row and column indices for sequences.

template <typename... Sequences>
using sequence_linear_row = variadic::adjoin_sequence<variadic::enumerate_sequence<variadic::sequence_size<Sequences>::value, 0, 0>...>;

template <typename... Sequences>
using sequence_linear_column = template_cat<variadic::sequence_of<Sequences>...>;

namespace detail
{

template <typename... S>
struct sequence_of_trait
{
    using row_type = sequence_row<sizeof...(S), sequence_size<variadic_front<S...>>::value>;
    using column_type = sequence_column<sizeof...(S), sequence_size<variadic_front<S...>>::value>;
};

// Specialization for custom columns

template <size_t... Columns, typename... S>
struct sequence_of_trait<sequence<Columns...>, S...>
{
    using row_type = sequence_row<sizeof...(S), sizeof...(Columns)>;
    using column_type = sequence_column_select<sizeof...(S), sequence<Columns...>>;
};

} // namespace detail

template <typename... Sequences>
using sequence_zip_row = typename detail::sequence_of_trait<Sequences...>::row_type;

template <typename... Sequences>
using sequence_zip_column = typename detail::sequence_of_trait<Sequences...>::column_type;

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_SEQUENCE_HPP
