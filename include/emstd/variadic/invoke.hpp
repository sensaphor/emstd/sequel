///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_INVOKE_HPP
#define EMSTD_VARIADIC_INVOKE_HPP

#include <emstd/detail/config.hpp>
#include <emstd/fox/invoke.hpp>
#include <emstd/std/tuple.hpp>
#include <emstd/core/tuplean.hpp>
#include <emstd/core/tuple_apply.hpp>
#include <emstd/variadic/arity.hpp>
#include <emstd/variadic/sequence.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// Invokes given function with all arguments.

EMSTD_INLINE_VARIABLE struct
{
    template <typename F, typename... Args>
    constexpr auto operator()(F&& fn, Args&&... args) const
        -> decltype(fox::invoke(declare<F>(), declare<Args>()...))
    {
        return fox::invoke(forward_cast<F>(fn), forward_cast<Args>(args)...);
    }
} invoke;

//-----------------------------------------------------------------------------
// Invokes given function with selected arguments.
//
// Examples: Invoke unary function with front element
//
//    invoke_at(f1, sequence<0>{}, a, b, c) becomes
//
//    f1(a)
//
// Examples: Invoke binary function with front and back element
//
//    invoke_at(f2, sequence<0, 2>{}, a, b, c) becomes
//
//    f2(a, c)

// Examples: Invoke ternary function with reversed elements
//
//    invoke_at(f3, sequence<2, 1, 0>{}, a, b, c) becomes
//
//    f3(c, b, a)

EMSTD_INLINE_VARIABLE struct
{
    template <typename F, size_t... I, typename... Args>
    constexpr auto operator()(F&& fn, sequence<I...> seq, Args&&... args) const
        -> decltype(core::tuple_apply_index(declare<F>(),
                                            declare<sequence<I...>>(),
                                            core::forward_as_tuple_rebind<core::tuplean>(declare<Args>()...)))
    {
        return core::tuple_apply_index(forward_cast<F>(fn),
                                       seq,
                                       core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }
} invoke_index;

//-----------------------------------------------------------------------------
// Applies second function (projection) to selected variadic slices and
// combines the results with the first function.

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              typename Project,
              size_t Arity,
              size_t... Index,
              typename... Args>
    constexpr auto operator()(F&& fn, Project&& proj, arity<Arity>, sequence<Index...>, Args&&... args) const
        -> decltype(fox::invoke(declare<F>(),
                                invoke_index(declare<Project>(),
                                             enumerate_sequence<Arity, Index>{},
                                             declare<Args>()...)...))
    {
        return fox::invoke(forward_cast<F>(fn),
                           invoke_index(forward_cast<Project>(proj),
                                        enumerate_sequence<Arity, Index>{},
                                        forward_cast<Args>(args)...)...);
    }
} invoke_over_index{};

//-----------------------------------------------------------------------------
// Applies second function (projection) to each variadic slice and combines
// the results with the first function.
//
// An optional function arity for the second function may be specified
// immediately after the functions. If omitted, the arity is deduced from the
// projection function.
//
// The projection arguments are consumed left-to-right.
//
// Examples: Invoke with unary projection
//
//    invoke_over(f2, g, a, b) becomes
//
//    f2(g(a), g(b))
//
// Examples: Invoke with binary projection
//
//    invoke_over(f2, g2, a1, a2, b1, b2) or
//    invoke_over(f2, g2, arity<2>{}, a1, a2, b1, b2) becomes
//
//    f2(g2(a1, a2), g2(b1, b2))

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              typename Project,
              size_t Arity,
              typename... Args,
              typename Sequence = enumerate_sequence<sizeof...(Args) / Arity, 0, Arity>>
    constexpr auto operator()(F&& fn, Project&& proj, const arity<Arity>&, Args&&... args) const
        -> decltype(invoke_over_index(declare<F>(),
                                      declare<Project>(),
                                      declare<arity<Arity>>(),
                                      declare<Sequence>(),
                                      declare<Args>()...))
    {
        return invoke_over_index(forward_cast<F>(fn),
                                 forward_cast<Project>(proj),
                                 arity<Arity>{},
                                 Sequence{},
                                 forward_cast<Args>(args)...);
    }

    template <typename F,
              typename Project,
              typename... Args,
              size_t Arity = arity_of<Project, Args...>::at(0),
              typename Sequence = enumerate_sequence<sizeof...(Args) / Arity, 0, Arity>,
              constrains_if<negation<core::is_arity<remove_cvref_t<variadic_front<Args...>>>>>* = nullptr>
    constexpr auto operator()(F&& fn, Project&& proj, Args&&... args) const
        -> decltype(invoke_over_index(declare<F>(),
                                      declare<Project>(),
                                      declare<arity<Arity>>(),
                                      declare<Sequence>(),
                                      declare<Args>()...))
    {
        return invoke_over_index(forward_cast<F>(fn),
                                 forward_cast<Project>(proj),
                                 arity<Arity>{},
                                 Sequence{},
                                 forward_cast<Args>(args)...);
    }
} invoke_over{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_INVOKE_HPP
