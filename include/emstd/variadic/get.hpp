///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_GET_HPP
#define EMSTD_VARIADIC_GET_HPP

#include <emstd/std/tuple.hpp>
#include <emstd/core/tuplean.hpp>
#include <emstd/ext/get.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// get
//
// Returns element at position Index in parameter pack.
//
// Index starts at zero.
//
// Examples:
//
//   get(index_constant<0>{}, true, 2, 3.0) returns bool with value true
//   get(index_constant<1>{}, true, 2, 3.0) returns int with value 2
//   get(index_constant<2>{}, true, 2, 3.0) returns double with value 3.0

EMSTD_INLINE_VARIABLE struct
{
    template <size_t Index, typename... Args>
    constexpr auto operator()(index_constant<Index>, Args&&... args) const
        -> variadic_element<Index, Args...>
    {
        return ext::get<Index>(core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }
} get{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_GET_HPP
