///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_VARIADIC_TRANSFORM_HPP
#define EMSTD_VARIADIC_TRANSFORM_HPP

#include <emstd/detail/config.hpp>
#include <emstd/detail/forward.hpp>
#include <emstd/std/tuple.hpp>
#include <emstd/core/tuplean.hpp>
#include <emstd/variadic/sequence.hpp>
#include <emstd/variadic/tuple_transform.hpp>

namespace emstd
{
namespace variadic
{

//-----------------------------------------------------------------------------
// Applies given function to selected variadic slices.
//
// The given variadic arguments are split into slices whose size equals the
// function arity.
//
// The slices are selected by integer sequences containing the start index of
// each slice.
//
// Applies the given function to each selected slice, assigns the result to
// the element specified by the associated output sequence; if the output
// sequence is omitted, the results are assigned to the front element in
// the associated slice.
//
// The parameters are:
//
// - The first parameter is the transform function.
//
// - The optional second parameter is the function arity. If omitted, the arity
//   is deduced from the function.
//
// - The third parameter is the input sequence containing the start of each
//   selected slice.
//
// - The optional fourth parameter is the output sequence where results are
//   assigned to.
//
// - The remaining parameter consists of variadic slices.

EMSTD_INLINE_VARIABLE struct
{

    template <typename F,
              size_t... Input,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, sequence<Input...> seq, Args&&... args) const
    {
        return tuple_transform_index(forward_cast<F>(fn),
                                     core::arity_of<F, Args...>{},
                                     seq,
                                     core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }

    template <typename F,
              size_t Arity,
              size_t... Input,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<Arity> width, sequence<Input...> seq, Args&&... args) const
    {
        return tuple_transform_index(forward_cast<F>(fn),
                                     width,
                                     seq,
                                     core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }

    template <typename F,
              size_t Arity,
              size_t... Input,
              size_t... Output,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<Arity> width, sequence<Input...> seq, sequence<Output...> outseq, Args&&... args) const
    {
        return tuple_transform_index(forward_cast<F>(fn),
                                     width,
                                     seq,
                                     outseq,
                                     core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }

    template <typename F,
              size_t... Output,
              size_t... Input,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, sequence<Input...> seq, sequence<Output...> outseq, Args&&... args) const
    {
        return tuple_transform_index(forward_cast<F>(fn),
                                     core::arity_of<F, Args...>{},
                                     seq,
                                     outseq,
                                     core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }
} transform_index{};

//-----------------------------------------------------------------------------
// Applies given function to each variadic slice.
//
// The parameters are:
//
// 1. The first parameter is the transform function.
//
// 2. The second parameter is an optional function arity. If omitted, the arity
//    is deduced from the function.
//
// 3. The remaining parameters are the variadic arguments, split into variadic
//    slices whose size equals the function arity.
//
// The given function is applied to each variadic slice and assigns the result
// to the front element of the current slice.
//
// Mandates:
// - The front element in each variadic slices is a mutable lvalue reference.
// - The number of variadic arguments is a multiple of the function arity.
//
// Example: Unary transform of three arguments.
//
//   transform(f1, a, b, c);
//
//   becomes
//
//   a = f1(a);
//   b = f1(b);
//   c = f1(c);
//
// Example: Binary transform on three slices with two elements each.
//
//   transform(f2, arity_2, a0, b0, a1, b1, a2, b2);
//
//   becomes
//
//   a0 = f2(a0, b0);
//   a1 = f2(a1, b1);
//   a2 = f2(a2, b2);
//
// Example: Ternary transform on two slices with three elements each.
//
//   transform(f3, arity_3, a0, b0, c0, a1, b1, c1);
//
//   becomes
//
//   a0 = f3(a0, b0, c0);
//   a1 = f3(a1, b1, c1);

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              size_t Arity,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<Arity> width, Args&&... args) const
    {
        static_assert((sizeof...(Args) % Arity) == 0, "Number of arguments must be a multiple of the function arity");

        return transform_index(forward_cast<F>(fn),
                               width,
                               enumerate_sequence<sizeof...(Args) / Arity, 0, Arity>{},
                               forward_cast<Args>(args)...);
    }

    template <typename F,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, Args&&... args) const
    {
        return (*this)(forward_cast<F>(fn),
                       core::arity_of<F, Args...>{},
                       forward_cast<Args>(args)...);
    }
} transform{};

//-----------------------------------------------------------------------------
// Applies given function to each variadic slice.
//
// N is the function arity.
// M is the number of variadic slices.
//
// The parameters are:
//
// 1. The first parameter is the function.
//
// 2. The second parameter is an optional function arity. If omitted, the arity
//    is deduced from the function.
//
// 3. The subsequent M parameters is the result slice, whose elements must be
//    mutable lvalue references where the results will be stored.
//
// 4. The remaining parameters consist of M variadic slices with N elements each.
//
// The elements of the variadic slices (3 and 4 above) are collectively called
// the data arguments.
//
// An N-ary transform thus splits the data arguments into 1 + M variadic
// slices of width N.
//
// The data arguments are consumed left-to-right.
//
// Examples: Unary transform of all arguments
//
//   transform_out(f, ra, rb, a, b) becomes
//
//   ra = f(a)
//   rb = f(b)
//
// Examples: Binary transform on three slices with two elements each
//
//   transform_out(f2, arity_2, ra, rb, rc, a1, a2, b1, b2, c1, c2);
//
//   becomes
//
//   ra = f2(a1, a2);
//   rb = f2(b1, b2);
//   rc = f2(c1, c2);
//
// Exampes: Ternary transform on two slices with three elements each
//
//   transform_out(f3, arity_3, ra, rb, a1, a2, a3, b1, b2, b3);
//
//   becomes
//
//   ra = f2(a1, a2, a3);
//   rb = f2(b1, b2, b3);
//
// Mandates:
// - The first M data arguments are mutable lvalue references.
// - sizeof...(Args) == N * (1 + M)

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              size_t N,
              typename... Args,
              size_t M = sizeof...(Args) / (1 + N)>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, core::arity<N> width, Args&&... args) const
    {
        static_assert((sizeof...(Args) % M) == 0, "Number of arguments must be a multiple of one plus the function arity");

        return transform_index(forward_cast<F>(fn),
                               width,
                               enumerate_sequence<M, M, N>{},
                               enumerate_sequence<M>{},
                               forward_cast<Args>(args)...);
    }

    template <typename F,
              typename... Args>
    EMSTD_ATTRIBUTE_FLATTEN
    constexpr voider operator()(F&& fn, Args&&... args) const
    {
        return (*this)(forward_cast<F>(fn),
                       core::arity_of<F, Args...>{},
                       forward_cast<Args>(args)...);
    }
} transform_onto{};

} // namespace variadic
} // namespace emstd

#endif // EMSTD_VARIADIC_TRANSFORM_HPP
