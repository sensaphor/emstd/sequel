///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_TACIT_APPLY_HPP
#define EMSTD_TACIT_APPLY_HPP

#include <emstd/std/invoke.hpp>
#include <emstd/ext/get.hpp>
#include <emstd/std/tuple.hpp>
#include <emstd/core/tuplean.hpp>
#include <emstd/variadic/sequence.hpp>
#include <emstd/variadic/invoke.hpp>

namespace emstd
{
namespace tacit
{

//-----------------------------------------------------------------------------
// apply_sequence

EMSTD_INLINE_VARIABLE struct
{
private:
    template <size_t... Row,
              size_t... Column,
              typename F,
              typename Tuple>
    constexpr auto invoke_tuple(variadic::sequence<Row...>, variadic::sequence<Column...>, F&& fn, Tuple&& tuple) const
        -> decltype(variadic::invoke(declare<F>(),
                                     declare<decltype(ext::get<Column>(ext::get<Row>(declare<Tuple>())))>()...))
    {
        return variadic::invoke(forward_cast<F>(fn),
                                ext::get<Column>(ext::get<Row>(forward_cast<Tuple>(tuple)))...);
    }

public:
    // Expands selected elements in specified order.

    template <size_t... Index,
              typename F,
              typename Tuple>
    constexpr auto operator()(variadic::sequence<Index...>, F&& fn, Tuple&& tuple) const
        -> decltype(variadic::invoke(declare<F>(),
                                     declare<decltype(ext::get<Index>(declare<Tuple>()))>()...))
    {
        return variadic::invoke(forward_cast<F>(fn),
                                ext::get<Index>(tuple)...);
    }

    // Expands selected elements of multiple tuples in specified order.

    template <size_t... Row,
              size_t... Column,
              typename F,
              typename... Args>
    constexpr auto operator()(variadic::sequence<Row...> rows, variadic::sequence<Column...> columns, F&& fn, Args&&... args) const
        -> decltype(invoke_tuple(declare<decltype(rows)>(),
                                 declare<decltype(columns)>(),
                                 declare<F>(),
                                 declare<core::tuplean<forward_cast_t<Args>...>>()))
    {
        return invoke_tuple(rows, columns, forward_cast<F>(fn), core::forward_as_tuple_rebind<core::tuplean>(forward_cast<Args>(args)...));
    }
} apply_sequence{};

//-----------------------------------------------------------------------------
// apply
//
// Expands tuples as linear parameter pack.

EMSTD_INLINE_VARIABLE struct
{
    // Expands all elements in increasing order.

    template <typename F,
              typename... Args,
              typename RowSequence = variadic::sequence_linear_row<remove_cvref_t<Args>...>,
              typename ColumnSequence = variadic::sequence_linear_column<remove_cvref_t<Args>...>>
    constexpr auto operator()(F&& fn, Args&&... args) const
        -> decltype(apply_sequence(declare<RowSequence>(), declare<ColumnSequence>(), declare<F>(), declare<Args>()...))
    {
        return apply_sequence(RowSequence{},
                              ColumnSequence{},
                              forward_cast<F>(fn),
                              forward_cast<Args>(args)...);
    }
} apply{};

//-----------------------------------------------------------------------------
// apply_zip
//
// Expands tuples as transposed parameter pack.

EMSTD_INLINE_VARIABLE struct
{
    // Expands all pairwise sequence elements in transposed order.
    //
    // Constraints:
    // - Two or more arguments.

    template <typename F,
              typename... Args,
              typename RowSequence = variadic::sequence_zip_row<remove_cvref_t<Args>...>,
              typename ColumnSequence = variadic::sequence_zip_column<remove_cvref_t<Args>...>>
    constexpr auto operator()(F&& fn, Args&&... args) const
        -> enable_if_t<(sizeof...(Args) >= 2),
                       decltype(apply_sequence(declare<RowSequence>(), declare<ColumnSequence>(), declare<F>(), declare<Args>()...))>
    {
        return apply_sequence(RowSequence{},
                              ColumnSequence{},
                              forward_cast<F>(fn),
                              forward_cast<Args>(args)...);
    }

    template <typename F,
              size_t... Select,
              typename... Args,
              typename RowSequence = variadic::sequence_zip_row<variadic::sequence<Select...>, remove_cvref_t<Args>...>,
              typename ColumnSequence = variadic::sequence_zip_column<variadic::sequence<Select...>, remove_cvref_t<Args>...>>
    constexpr auto operator()(F&& fn, variadic::sequence<Select...>, Args&&... args) const
        -> enable_if_t<(sizeof...(Args) >= 2),
                       decltype(apply_sequence(declare<RowSequence>(), declare<ColumnSequence>(), declare<F>(), declare<Args>()...))>
    {
        return apply_sequence(RowSequence{},
                              ColumnSequence{},
                              forward_cast<F>(fn),
                              forward_cast<Args>(args)...);
    }
} apply_zip{};

} // namespace tacit
} // namespace emstd

#endif // EMSTD_TACIT_APPLY_HPP
