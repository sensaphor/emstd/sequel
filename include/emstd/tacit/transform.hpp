///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_TACIT_TRANSFORM_HPP
#define EMSTD_TACIT_TRANSFORM_HPP

#include <emstd/std/bind_front.hpp>
#include <emstd/tacit/apply.hpp>
#include <emstd/variadic/transform.hpp>

namespace emstd
{
namespace tacit
{

//-----------------------------------------------------------------------------
// Applies function to each tuple element group.
//
// The element group size equals the function arity.
//
// Multiple tuples are processed as if they are concatenated.
//
// Mandates:
// - The total number of elements must be a multiple of the function arity.
// - The result size must be equal to the total number of elements divided
//   by the function arity.
//
// Examples: Unary transform
//
//   const int first[]{ 11, 22, 33 };
//
//   transform(f1, result, first) or
//   transform(f1, arity_1, result, first) becomes
//
//   result[0] = f1(first[0]); // f1(11)
//   result[1] = f1(first[1]); // f1(22)
//   result[2] = f1(first[2]); // f1(33)
//
// Examples: Binary transform
//
//   const int first[] { 11, 22, 33 };
//   const int second[]{ 44, 55, 66 };
//
//   transform(f2, result, first, second) or
//   transform(f2, arity_2, result, first, second) becomes
//
//   result[0] = f2(first[0], first[1]);   // f2(11, 22)
//   result[1] = f2(first[2], second[0]);  // f2(33, 44)
//   result[2] = f2(second[1], second[2]); // f2(55, 66)

EMSTD_INLINE_VARIABLE struct
{
private:
    template <typename Head, typename... Tail>
    using linear_rows = variadic::adjoin_sequence<variadic::sequence_linear_row<Head>, variadic::sequence_linear_row<Tail...>>;

    template <typename Head, typename... Tail>
    using linear_columns = template_cat<variadic::sequence_linear_column<Head>, variadic::sequence_linear_column<Tail...>>;

public:
    template <typename F,
              typename R,
              typename... Args>
    constexpr voider operator()(F&& fn, R& result, Args&&... args) const
    {
        return apply_sequence(linear_rows<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              linear_columns<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              core::bind_front(variadic::transform_onto, forward_cast<F>(fn)),
                              result,
                              forward_cast<Args>(args)...);
    }

    template <typename F,
              size_t Arity,
              typename R,
              typename... Args>
    constexpr voider operator()(F&& fn, variadic::arity<Arity> r, R& result, Args&&... args) const
    {
        static_assert(sizeof...(Args) % Arity == 0, "The number of arguments must be a multiple of the function arity");

        return apply_sequence(linear_rows<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              linear_columns<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              core::bind_front(core::bind_front(variadic::transform_onto, forward_cast<F>(fn)), r),
                              result,
                              forward_cast<Args>(args)...);
    }
} transform{};

//-----------------------------------------------------------------------------
// Applies function to each zipped (transposed) element group.
//
// The element group size equals the function arity.
//
// Multiple tuples are processed piecewise.
//
// Applies the given function to the Ith element in each tuple and repeat for
// every element in the tuple.
//
// Constraints:
// - There must be at least two tuples.
//
// Mandates:
// - The number of tuples must be equal to the function arity.
// - All tuples must have equal size.
// - The result size must be equal to the tuple size.
//
// Examples: Binary zipped transform
//
//   const int first[] { 11, 22, 33 };
//   const int second[]{ 44, 55, 66 };
//
//   transform_zip(f2, result, first, second) or
//   transform_zip(f2, arity_2, result, first, second) becomes
//
//   result[0] = f2(first[0], second[0]); // f2(11, 44)
//   result[1] = f2(first[1], second[1]); // f2(22, 55)
//   result[2] = f2(first[2], second[2]); // f2(33, 66)
//
// Examples: Ternary zipped transform
//
//   const int first[] {  11,  22,  33 };
//   const int second[]{  44,  55,  66 };
//   const int third[] { 100, 200, 300 };
//
//   transform_zip(f3, result, first, second, third) or
//   transform_zip(f3, arity_3, result, first, second, third) becomes
//
//   result[0] = f3(first[0], second[0], third[0]); // f3(11, 44, 100)
//   result[1] = f3(first[1], second[1], third[1]); // f3(22, 55, 200)
//   result[2] = f3(first[2], second[2], third[2]); // f3(33, 66, 300)

EMSTD_INLINE_VARIABLE struct
{
private:
    template <typename Head, typename... Tail>
    using zip_rows = variadic::adjoin_sequence<variadic::sequence_linear_row<Head>, variadic::sequence_zip_row<Tail...>>;

    template <typename Head, typename... Tail>
    using zip_columns = template_cat<variadic::sequence_linear_column<Head>, variadic::sequence_zip_column<Tail...>>;

public:
    template <typename F,
              typename R,
              typename... Args>
    constexpr auto operator()(F&& fn, R& result, Args&&... args) const
        -> enable_if_t<(sizeof...(Args) >= 2), voider>
    {
        return apply_sequence(zip_rows<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              zip_columns<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              core::bind_front(variadic::transform_onto, forward_cast<F>(fn)),
                              result,
                              forward_cast<Args>(args)...);
    }

    template <typename F,
              size_t Arity,
              typename R,
              typename... Args>
    constexpr auto operator()(F&& fn, variadic::arity<Arity> r, R& result, Args&&... args) const
        -> enable_if_t<(sizeof...(Args) >= 2), voider>
    {
        static_assert(Arity == sizeof...(Args), "The number of arguments must match the function arity");

        return apply_sequence(zip_rows<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              zip_columns<remove_cvref_t<R>, remove_cvref_t<Args>...>{},
                              core::bind_front(core::bind_front(variadic::transform_onto, forward_cast<F>(fn)), r),
                              result,
                              forward_cast<Args>(args)...);
    }
} transform_zip{};

} // namespace tacit
} // namespace emstd

#endif // EMSTD_TACIT_TRANSFORM_HPP
