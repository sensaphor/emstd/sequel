///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_TACIT_ARITHMETIC_HPP
#define EMSTD_TACIT_ARITHMETIC_HPP

#include <emstd/std/bind_front.hpp>
#include <emstd/variadic/arithmetic.hpp>
#include <emstd/tacit/apply.hpp>

namespace emstd
{
namespace tacit
{

//-----------------------------------------------------------------------------
// Addition (sum) of tuple elements.

EMSTD_INLINE_VARIABLE auto add = core::bind_front(tacit::apply, variadic::add);

//-----------------------------------------------------------------------------
// Subtraction of tuple elements.

EMSTD_INLINE_VARIABLE auto subtract = core::bind_front(tacit::apply, variadic::subtract);

//-----------------------------------------------------------------------------
// Multiplication (product) of tuple elements.

EMSTD_INLINE_VARIABLE auto multiply = core::bind_front(tacit::apply, variadic::multiply);

//-----------------------------------------------------------------------------
// Dot product of parameter pack.
//
// Mandates:
// - Two arguments.
//
// Examples:
//
//   const int first[]{ 1, 2, 3 };
//   const int second[]{ 10, 20, 30 };
//
//   tacit::dot_product(lhs, rhs) == 1 * 10 + 2 * 20 + 3 * 30

EMSTD_INLINE_VARIABLE auto dot_product = core::bind_front(tacit::apply_zip, variadic::dot_product);

//-----------------------------------------------------------------------------
// Cumulative addition (sum) of tuple elements.

EMSTD_INLINE_VARIABLE auto partial_add = core::bind_front(tacit::apply, variadic::partial_add);

//-----------------------------------------------------------------------------
// Cumulative multiplication (product) of tuple elements.

EMSTD_INLINE_VARIABLE auto partial_multiply = core::bind_front(tacit::apply, variadic::partial_multiply);

} // namespace tacit
} // namespace emstd

#endif // EMSTD_TACIT_ARITHMETIC_HPP
