///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_TACIT_FOLD_HPP
#define EMSTD_TACIT_FOLD_HPP

#include <emstd/detail/config.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/tacit/apply.hpp>
#include <emstd/variadic/fold.hpp>

namespace emstd
{
namespace tacit
{

//-----------------------------------------------------------------------------
// Left fold over elements in given tuple-like container.

EMSTD_INLINE_VARIABLE struct
{
    template <typename F,
              typename T>
    constexpr auto operator()(F&& fn, T&& t) const
        -> decltype(apply(core::bind_front(variadic::fold, declare<F>()),
                          declare<T>()))
    {
        return apply(core::bind_front(variadic::fold, forward_cast<F>(fn)),
                     forward_cast<T>(t));
    }

    template <typename F,
              size_t... Index,
              typename T>
    constexpr auto operator()(F&& fn, variadic::sequence<Index...>, T&& t) const
        -> decltype(apply_sequence(declare<variadic::sequence<Index...>>(),
                                   core::bind_front(variadic::fold, declare<F>()),
                                   declare<T>()))
    {
        return apply_sequence(variadic::sequence<Index...>{},
                              core::bind_front(variadic::fold, forward_cast<F>(fn)),
                              forward_cast<T>(t));
    }
} fold;

//-----------------------------------------------------------------------------
// Applies second function (projection) to each paired element of the two
// given tuple-like containers, and left folds the results using the first
// function.

EMSTD_INLINE_VARIABLE struct
{
    template <typename Fold,
              typename Projection,
              typename Lhs,
              typename Rhs>
    constexpr auto operator()(Fold&& fold, Projection&& projection, Lhs&& lhs, Rhs&& rhs) const
        -> decltype(apply_zip(core::bind_front(variadic::fold_over, declare<Fold>(), declare<Projection>()),
                              declare<Lhs>(),
                              declare<Rhs>()))
    {
        return apply_zip(core::bind_front(variadic::fold_over, forward_cast<Fold>(fold), forward_cast<Projection>(projection)),
                         forward_cast<Lhs>(lhs),
                         forward_cast<Rhs>(rhs));
    }

    template <typename Fold,
              typename Projection,
              size_t... Select,
              typename Lhs,
              typename Rhs>
    constexpr auto operator()(Fold&& fold, Projection&& projection, variadic::sequence<Select...> seq, Lhs&& lhs, Rhs&& rhs) const
        -> decltype(apply_zip(core::bind_front(variadic::fold_over, declare<Fold>(), declare<Projection>()),
                              declare<decltype(seq)>(),
                              declare<Lhs>(),
                              declare<Rhs>()))
    {
        return apply_zip(core::bind_front(variadic::fold_over, forward_cast<Fold>(fold), forward_cast<Projection>(projection)),
                         seq,
                         forward_cast<Lhs>(lhs),
                         forward_cast<Rhs>(rhs));
    }
} fold_over;

} // namespace tacit
} // namespace emstd

#endif // EMSTD_TACIT_FOLD_HPP
