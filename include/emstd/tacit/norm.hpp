///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2024 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_TACIT_NORM_HPP
#define EMSTD_TACIT_NORM_HPP

#include <emstd/detail/config.hpp>
#include <emstd/std/bind_front.hpp>
#include <emstd/variadic/norm.hpp>
#include <emstd/tacit/apply.hpp>

namespace emstd
{
namespace tacit
{

//-----------------------------------------------------------------------------
// Maximum tuple element.

EMSTD_INLINE_VARIABLE auto max = core::bind_front(tacit::apply, variadic::max);

//-----------------------------------------------------------------------------
// Cumulative maximum tuple element.

EMSTD_INLINE_VARIABLE auto partial_max = core::bind_front(tacit::apply, variadic::partial_max);

//-----------------------------------------------------------------------------
// Minimum tuple element.

EMSTD_INLINE_VARIABLE auto min = core::bind_front(tacit::apply, variadic::min);

//-----------------------------------------------------------------------------
// Cumulative minimum tuple element.

EMSTD_INLINE_VARIABLE auto partial_min = core::bind_front(tacit::apply, variadic::partial_min);

//-----------------------------------------------------------------------------
// Minimum and maximum tuple element.

EMSTD_INLINE_VARIABLE auto minmax = core::bind_front(tacit::apply, variadic::minmax);

} // namespace tacit
} // namespace emstd

#endif // EMSTD_TACIT_NORM_HPP
